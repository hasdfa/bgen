package bgen

import (
	"bitbucket.org/hasdfa/bgen/internal"
	"bitbucket.org/hasdfa/bgen/pkg/dbUtils"
	"github.com/kataras/iris/context"
	"github.com/valyala/fasthttp"
	"gopkg.in/go-playground/validator.v9"
	"net/http"
)

type (
	Context interface {
		GetHeader(key string) string
		GetQuery(key string) string
		GetPathParam(key string) string

		Request() *http.Request
		ContentType() string
		ParseBody(i interface{}) error

		SetResponseHeader(key, value string)
		RemoveResponseHeader(key string)

		SetAuthenticated(i interface{})
		IsAuthenticated() bool
		GetAuthenticated() interface{}

		Repository() dbUtils.DatabaseRepository
	}
)

func NewContext(w http.ResponseWriter, r *http.Request) *internal.ContextImpl {
	return &internal.ContextImpl{
		HttpRequest:    r,
		Writer:         w,
		Status:         http.StatusInternalServerError,
		Repo:           SharedRepositories,
		Valid:          validator.New(),
		RawContentType: DefaultResponseContentType,
	}
}

func NewContextFromIris(c context.Context) *internal.IrisContextImpl {
	return &internal.IrisContextImpl{
		Context:        c,
		Status:         http.StatusInternalServerError,
		Repo:           SharedRepositories,
		Valid:          validator.New(),
		RawContentType: DefaultResponseContentType,
	}
}

func NewFasthttpContext(ctx *fasthttp.RequestCtx) *internal.FasthttpContextImpl {
	return &internal.FasthttpContextImpl{
		Context:        ctx,
		Status:         http.StatusInternalServerError,
		Repo:           SharedRepositories,
		Valid:          validator.New(),
		RawContentType: DefaultResponseContentType,
	}
}
