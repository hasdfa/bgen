package parsing

import (
	"bitbucket.org/hasdfa/bgen/codegen/golang"
	"go/parser"
	"go/token"
)

func ValidationError(cc golang.CodeConvertible) error {
	return CodeValidationError(cc.Convert())
}

func CodeValidationError(code string) error {
	fs := token.NewFileSet()
	_, err := parser.ParseFile(fs, "", code, parser.AllErrors)

	return err
}

func IsValidCode(cc golang.CodeConvertible) bool {
	return ValidationError(cc) == nil
}
