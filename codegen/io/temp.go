package io

import (
	"fmt"
	"io"
	"reflect"
)

type memoryWriter struct {
	buffer []byte
}

func (w *memoryWriter) Write(p []byte) (int, error) {
	w.buffer = append(w.buffer, p...)
	return 0, nil
}

func (w *memoryWriter) Close() error {
	return nil
}

func NewTempWriter() io.WriteCloser {
	return &memoryWriter{}
}

func ReadAllWroteBytes(w io.WriteCloser) []byte {
	if iw, ok := w.(*memoryWriter); ok {
		return iw.buffer
	}
	panic(fmt.Sprintf("Wrong type: %s, must be *io.memoryWriter", reflect.TypeOf(w)))
}
