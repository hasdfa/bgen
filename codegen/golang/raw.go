package golang

import (
	"fmt"
	"os"
	"path"
	"strings"
)

type GoRawFile struct {
	Name        string
	Method      string
	FullPackage string

	Raw string
}

func (f *GoRawFile) Package() string {
	spl := strings.Split(f.FullPackage, "/")
	return spl[len(spl)-1]
}

func (f *GoRawFile) path() string {
	return path.Join(os.Getenv("GOPATH"), "src", f.FullPackage)
}

func (f *GoRawFile) Filepath() string {
	if f.Method == "" {
		return path.Join(f.path(), fmt.Sprintf("%s.go", f.Name))
	}
	return path.Join(f.path(), fmt.Sprintf("%s:%s.go", strings.ToLower(f.Method), f.Name))
}

func (f *GoRawFile) Save() error {
	p := f.Filepath()

	fl, err := os.Create(p)
	if err != nil {
		return err
	}

	if _, err = fl.WriteString("package " + f.Package() + "\n\n" + f.Raw); err != nil {
		return err
	}

	return nil
}

func (f *GoRawFile) Delete() error {
	return os.Remove(f.Filepath())
}

func (f *GoRawFile) Refactor() (err error) {
	p := f.Filepath()

	if err = runCmd("goimports", "-w", p); err != nil {
		if strings.Contains(err.Error(), `"goimports": executable file not found`) {
			fmt.Print(`We need goimports tool to work. Install it?
Answer (y/n): `)

			var str string
			_, _ = fmt.Scanln(&str)

			if DoNotAsk() || strings.ToLower(str) == "y" {
				fmt.Println("go get -u golang.org/x/tools/cmd/goimports")
				if err = runCmd("go", "get", "-v", "-u", "golang.org/x/tools/cmd/goimports"); err != nil {
					return err
				}
				return f.Refactor()
			} else {
				fmt.Println("You can download it mannually: `go get -u golang.org/x/tools/cmd/goimports`")
			}
		} else {
			return err
		}
	}
	if err = runCmd("gofmt", "-w", p); err != nil {
		return err
	}
	if err = runCmd("go", "vet", p); err != nil {
		return err
	}
	if err = runCmd("go", "tool", "fix", p); err != nil {
		return err
	}

	return nil
}

func (f *GoRawFile) Compile() error {
	defer runCmd("go", "clean")
	return runCmd("go", "tool", "compile", f.Filepath())
}
