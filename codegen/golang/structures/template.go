package structures

import (
	"bitbucket.org/hasdfa/bgen/codegen/golang/interfaces"
	"fmt"
	"strings"
)

type (
	Structures []*Structure

	Structure struct {
		Name   string
		Fields []*Field
		Funcs  []*interfaces.IFunc
	}

	Field struct {
		Name  string
		Type  string
		Value interface{}
		Tags  map[string]string
	}
)

func (obj *Structure) Realise(interfaces ...*interfaces.Interface) {
	for _, i := range interfaces {
		obj.Funcs = append(obj.Funcs, i.Functions...)
	}
}

func (obj *Structure) RealiseFuncs(funcs ...*interfaces.IFunc) {
	obj.Funcs = append(obj.Funcs, funcs...)
}

func (obj *Structure) Convert() string {
	str := "type " + strings.Title(obj.Name) + " struct {\n"

	for _, fl := range obj.Fields {
		str += "\t" + fl.Convert() + "\n"
	}
	str += "}\n"

	if len(obj.Funcs) > 0 {
		str += "\n\n"
	}
	for _, fn := range obj.Funcs {
		str += "func (obj *" + obj.Name + ") " + fn.Name + "(" + fn.ParamsString() + ") " + fn.ReturnString() + "{\n"
		for _, l := range strings.Split(fn.SourceCode(), "\n") {
			str += "\t" + l + "\n"
		}
		str += "}\n"
	}

	return str + "\n"
}

func (f *Field) Convert() string {
	tags := ""
	for k, v := range f.Tags {
		tags += fmt.Sprintf("%s:\"%s\" ", k, v)
	}

	return strings.Title(f.Name) + " " + f.Type + " `" + tags[:len(tags)-1] + "`"
}

func (f *Field) TypeValue() string {
	if f.Type == "string" {
		return fmt.Sprintf("\"%s\"", f.Value)
	}
	return fmt.Sprint(f.Value)
}

func (ss *Structures) Convert() string {
	str := "// Declaration of structures\n"
	for _, s := range *ss {
		str += s.Convert()
	}

	return str + "\n\n\n"
}

func (ss *Structures) Append(s *Structure) {
	*ss = append(*ss, s)
}

func (ss *Structures) Get(i int) *Structure {
	return (*ss)[i]
}

func (ss *Structures) Count() int {
	return len(*ss)
}
