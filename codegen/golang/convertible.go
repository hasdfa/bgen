package golang

type CodeConvertible interface {
	Convert() string
}