package golang

import (
	"bitbucket.org/hasdfa/bgen/codegen/golang/interfaces"
	"bitbucket.org/hasdfa/bgen/codegen/golang/structures"
	"strings"
)

type GoFile struct {
	Name        string
	FullPackage string
	Imports     []string

	Variables []*structures.Field
	Constants []*structures.Field
	Functions []*interfaces.IFunc

	Structures structures.Structures
	Interfaces interfaces.Interfaces
}

func (f *GoFile) Import(other *GoFile) {
	if f.path() != other.path() {
		f.Imports = append(f.Imports, other.FullPackage)
	}
}

func (f *GoFile) Package() string {
	spl := strings.Split(f.FullPackage, "/")
	return strings.Replace(spl[len(spl)-1], ".", "_", 100)
}

func parseFile(fileds []*structures.Field) string {
	str := ""
	for _, f := range fileds {
		str += "\t" + f.Name + " " + f.Type + " = " + f.TypeValue() + "\n"
	}
	return str
}

func (f *GoFile) Convert() string {
	file := "package " + f.Package() + "\n\n"

	file += "import (\n"
	for _, v := range f.Imports {
		file += "\t\"" + v + "\"\n"
	}
	file += ")\n\n"

	file += "const (\n"
	file += parseFile(f.Constants)
	file += ")\n\n"

	file += "var (\n"
	file += parseFile(f.Variables)
	file += ")\n\n"

	file += f.Interfaces.Convert()
	file += f.Structures.Convert()

	if len(f.Functions) > 0 {
		file += "\n\n"
	}
	for _, fn := range f.Functions {
		file += "func " + fn.Name + "(" + fn.ParamsString() + ") " + fn.ReturnString() + "{"
		for _, l := range strings.Split(fn.SourceCode(), "\n") {
			file += "\t" + l + "\n"
		}
		file += "}\n\n"
	}

	return file
}
