package interfaces

import (
	"strings"
)

type (
	Interfaces []*Interface

	Interface struct {
		Name      string
		Functions []*IFunc
	}

	IFunc struct {
		Name   string
		Code   string
		Params []*FuncParam
		Return []string
	}

	FuncParam struct {
		Name string
		Type string
	}
)


func (obj *Interface) Convert() string {
	str := "type " + strings.Title(obj.Name) + " interface {\n"

	for _, f := range obj.Functions {
		str += "\t" + f.Name + "(" + f.ParamsString() + ") " + f.ReturnString() + "\n"
	}
	return str + "}\n"
}

func (obj *IFunc) ParamsString() string {
	str := ""

	for _, f := range obj.Params {
		str += f.Name + " " + f.Type + ", "
	}
	return str[:len(str)-2]
}

func (obj *IFunc) ReturnString() string {
	if len(obj.Return) == 0 {
		return ""
	} else if len(obj.Return) == 1 {
		return obj.Return[0]
	} else {
		return "(" + strings.Join(obj.Return, ", ") + ")"
	}
}

func (obj *IFunc) SourceCode() string {
	if obj.Code == "" {
		return "panic(\"implement me\")"
	}
	return obj.Code
}


func (is *Interfaces) Convert() string {
	str := "// Declaration of interfaces\n"
	for _, i := range *is {
		str += i.Convert()
	}

	return str + "\n\n\n"
}

func (is *Interfaces) Append(i *Interface) {
	*is = append(*is, i)
}

func (is *Interfaces) Get(i int) *Interface {
	return (*is)[i]
}

func (is *Interfaces) Count() int {
	return len(*is)
}