package golang

import (
	"flag"
	"fmt"
	"os"
	"os/exec"
	"path"
	"strings"
)

var (
	_ = flag.String("dna", "", "Write -dna to DoNotAsk for download")
)

func DoNotAsk() bool {
	return flag.Lookup("dna") != nil
}

func (f *GoFile) path() string {
	return path.Join(os.Getenv("GOPATH"), "src", f.FullPackage)
}

func (f *GoFile) filepath() string {
	return path.Join(f.path(), f.Name+".go")
}

func (f *GoFile) Save() error {
	p := f.filepath()

	fl, err := os.Create(p)
	if err != nil {
		return err
	}

	if _, err = fl.WriteString(f.Convert()); err != nil {
		return err
	}

	return nil
}

func (f *GoFile) Delete() error {
	return os.Remove(f.filepath())
}

func (f *GoFile) Refactor() (err error) {
	p := f.filepath()

	if err = runCmd("gofmt", "-w", p); err != nil {
		return err
	}
	if err = runCmd("go", "tool", "vet", p); err != nil {
		return err
	}
	if err = runCmd("go", "tool", "fix", p); err != nil {
		return err
	}
	if err = runCmd("goimports", "-w", p); err != nil {
		if strings.Contains(err.Error(), `"goimports": executable file not found`) {
			fmt.Print(`We need goimports tool to work. Install it?
Answer (y/n): `)

			var str string
			fmt.Scanln(&str)

			if DoNotAsk() || strings.ToLower(str) == "y" {
				fmt.Println("go get -u golang.org/x/tools/cmd/goimports")
				if err = runCmd("go", "get", "-v", "-u", "golang.org/x/tools/cmd/goimports"); err != nil {
					return err
				}
				return f.Refactor()
			} else {
				fmt.Println("You can download it mannually: `go get -u golang.org/x/tools/cmd/goimports`")
			}
		} else {
			return err
		}
	}

	return nil
}

func (f *GoFile) Compile() error {
	defer runCmd("go", "clean", "")
	return runCmd("go", "tool", "compile", f.filepath())
}

// go get golang.org/x/tools/cmd/goimports
func runCmd(name string, args ...string) error {
	cmd := exec.Command(name, args...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	return cmd.Run()
}
