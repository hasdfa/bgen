package generator

import (
	"encoding/json"
	"fmt"
	"os"
	"path"
)

type (
	Documentation struct {
		Main        *DocMain              `json:"info"`
		Routes      []*DocPath            `json:"routes"`
		Middlewares map[string]*DocMiddle `json:"middlewares"`
	}

	DocMain struct {
		Project     string `json:"project"`
		Description string `json:"description,omitempty"`
	}

	DocPath struct {
		Path        string        `json:"path"`
		Method      string        `json:"method"`
		Middlewares []string      `json:"middlewares,omitempty"`
		Request     *DocObjParams `json:"request,omitempty"`
		Response    *DocObjParams `json:"response,omitempty"`
	}

	DocObjParams struct {
		All map[string]interface{} `json:"*,omitempty"`

		Json   map[string]interface{} `json:"json,omitempty"`
		Xml    map[string]interface{} `json:"xml,omitempty"`
		Query  map[string]interface{} `json:"query,omitempty"`
		Header map[string]interface{} `json:"header,omitempty"`
		Form   map[string]interface{} `json:"form,omitempty"`
	}

	DocMiddle struct {
		Description string      `json:"description,omitempty"`
		Headers     interface{} `json:"headers,omitempty"`
		Queries     interface{} `json:"queries,omitempty"`
	}
)

//var (
//	mainDocumentation = template.Must(template.New("doc-path").Parse(`# {{ .Package }} REST API documentation
//{{range .Routes}}
//## {{ .Method }} {{ .HttpPath }}
//{{ if .RequestParamJson }}# Request:
//{{ .RequestParamJson }}{{end}}
//# Response
//{{ .ResponseParamJson }}
//{{end}}
//`))
//)

func (s *Structure) MakeDocs() {
	docPath := path.Join(gopathSrc, s.Package, DOC_FILENAME)
	f, err := os.Create(docPath)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	bts, err := json.MarshalIndent(s.Docs, "", "\t")
	if err != nil {
		panic(err)
	}

	_, err = f.Write(bts)
	if err != nil {
		panic(err)
	}
	fmt.Println("Saved docs to " + docPath)
}
