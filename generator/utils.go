package generator

import (
	"fmt"
	"reflect"
	"strings"
)

type doc struct {
	saved map[string]map[string]map[string]interface{}
}

var (
	defaultValue = map[string]interface{}{
		"$object": "undefined",
	}
)

func valueOfType(tp reflect.Type) *DocObjParams {
	i, err := makeEmptyObjectOf(tp)
	if err != nil {
		return &DocObjParams{
			All: defaultValue,
		}
	}
	return i
}

var localType reflect.Type = nil

func makeEmptyObjectOf(tp reflect.Type) (i *DocObjParams, err interface{}) {
	defer func() {
		if e := recover(); e != nil {
			err = e
			fmt.Println(err)
			fmt.Println(localType.String())
			fmt.Println()
		}
	}()
	d := &doc{
		saved: make(map[string]map[string]map[string]interface{}),
	}

	if tp.Kind() == reflect.Interface {
		return
	}

	i = &DocObjParams{
		Json:   d.processFields("json", tp),
		Xml:    d.processFields("xml", tp),
		Query:  d.processFields("query", tp),
		Header: d.processFields("header", tp),
		Form:   d.processFields("form", tp),
	}

	return
}

func (d *doc) processFields(tag string, tp reflect.Type) map[string]interface{} {
	localType = tp
	if itm, ok := d.saved[tp.Name()]; ok {
		if inn, ok := itm[tag]; ok {
			return inn
		}
	}

	for tp.Kind() == reflect.Ptr {
		tp = tp.Elem()
	}
	obj := reflect.New(tp)
	if obj.Kind() == reflect.Ptr {
		obj = reflect.Indirect(obj)
	}
	if tp.Kind() == reflect.Interface {
		return defaultValue
	}

	mp := make(map[string]interface{})
	for i := 0; i < tp.NumField(); i++ {
		fo := obj.Field(i)
		ft := tp.Field(i)
		key := ft.Name
		isOptional := false

		if k, ok := ft.Tag.Lookup(tag); ok {
			if k == "-" || k == "" {
				continue
			} // ignore
			key = k

			arr := strings.Split(key, ",")
			for i, s := range arr {
				if i == 0 {
					key = s
				}

				if s == "omitempty" {
					isOptional = true
					break
				}
			}
			if !isOptional {
				if k, ok := ft.Tag.Lookup("validate"); ok {
					isOptional = strings.Contains(k, "omitempty")
				}
			}
		} else if k != "json" && k != "xml" {
			continue
		}
		key = strings.Split(key, ",")[0]

		switch fo.Kind() {
		case reflect.Array, reflect.Slice:
			elem := ft.Type.Elem()
			if elem.Kind() == reflect.Struct ||
				elem.Kind() == reflect.Interface ||
				elem.Kind() == reflect.Ptr {
				mp[key] = []map[string]interface{}{
					d.processFields(tag, elem),
				}
			} else {
				mp[key] = []string{elem.String()}
			}
			break
		case reflect.Map:
			elem := ft.Type.Elem()
			if elem.Kind() == reflect.Struct ||
				elem.Kind() == reflect.Interface ||
				elem.Kind() == reflect.Ptr {
				mp[key] = map[string]interface{}{
					ft.Type.Key().String(): d.processFields(tag, ft.Type.Elem()),
				}
			} else {
				mp[key] = map[string]interface{}{
					ft.Type.Key().String(): elem.String(),
				}
			}
			break
		case reflect.Struct, reflect.Ptr:
			mp[key] = d.processFields(tag, ft.Type)
			break
		default:
			if isOptional {
				mp[key] = ft.Type.String() + " (optional)"
			} else {
				mp[key] = ft.Type.String()
			}
		}
	}
	d.saved[tp.Name()] = map[string]map[string]interface{}{
		tag: mp,
	}
	return mp
}
