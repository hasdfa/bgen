package generator

import (
	"bitbucket.org/hasdfa/bgen/pkg/serviceUtils"
	"fmt"
	"reflect"
	"strings"
)

func ProcessRouter(controller serviceUtils.ConfigurableService, method, path string) (route interface{}) {
	defer func() {
		str := "nil"
		if route != nil {
			str = reflect.TypeOf(route).String()
		}
		fmt.Printf("Bind %s '%s' -> %s\n", method, path, str)
	}()

	parts := strings.Split(strings.TrimSuffix(
		strings.TrimPrefix(path, "/"), "/"), "/")

	for _, p := range parts {
		p = "/" + p

		if child := controller.Router().FindChild(p); child != nil {
			controller = child.(serviceUtils.ConfigurableService)
		} else if serving := controller.Router().FindFileServings(p); serving != nil {
			return serving
		} else {
			route = controller.Router().FindHandlers(method, p)
			if secure, ok := route.(*serviceUtils.SecurePath); ok {
				return secure.Dest
			} else if middle, ok := route.(*serviceUtils.MiddlewarePath); ok {
				return middle.Dest
			}
			return route
		}
	}
	return nil
}
