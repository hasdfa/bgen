package generator

type memoryWriter struct {
	bts []byte
}

func (w *memoryWriter) Write(p []byte) (n int, err error) {
	w.bts = append(w.bts, p...)
	return len(p), nil
}

