package generator

import (
	"bitbucket.org/hasdfa/bgen/codegen/golang"
	"bitbucket.org/hasdfa/bgen/pkg/middlewareUtils"
	"bitbucket.org/hasdfa/bgen/pkg/serviceUtils"
	"bitbucket.org/hasdfa/bgen/pkg/utils"
	"fmt"
	"os"
	"path"
	"reflect"
	"strings"
)

type Structure struct {
	Package string
	Source  []*FileObject
	Docs    *Documentation

	commonFiles     *CommonFile
	middlewares     middlewareUtils.MiddlewareDictionary
	rolesMiddleware *middlewareUtils.MiddlewareSource
}

func (s *Structure) Walk(basePath string, config *serviceUtils.ServiceConfiguration) {
	config.RangeFileServings(func(route string) {
		s.commonFiles.Servings = append(s.commonFiles.Servings, path.Join(basePath, route))
	})
	config.RangeMiddleware(func(middleware string) {
		s.commonFiles.addMiddleware(basePath, middleware)

		__m := s.middlewares[middleware]
		s.Docs.Middlewares[middleware] = &DocMiddle{
			Description: __m.Description,
			Headers:     __m.Headers,
			Queries:     __m.Queries,
		}
	})

	paths := makeGen(strings.Split(basePath, "/"))
	pkgGen := paths[len(paths)-1]
	filePath := path.Join(strings.Join(paths[:len(paths)-1], "/"), pkgGen)

	config.RangeHandlers(func(method, childPath string, route interface{}) {
		if secure, ok := route.(*serviceUtils.SecurePath); ok {
			s.commonFiles.RoleMiddlewares[method+":"+path.Join(basePath, childPath)] = secure.Role
			s.Docs.Middlewares[middlewareUtils.RoleKey] = &DocMiddle{
				Description: s.middlewares[middlewareUtils.RoleKey].Description,
			}
			route = secure.Dest
		} else if middlewarePath, ok := route.(*serviceUtils.MiddlewarePath); ok {
			route = middlewarePath.Dest
			s.commonFiles.addMiddleware(method+":"+path.Join(basePath, childPath), middlewarePath.MiddlewareKey)
			s.Docs.Middlewares[middlewarePath.MiddlewareKey] = &DocMiddle{
				Description: s.middlewares[middlewarePath.MiddlewareKey].Description,
			}
		}

		parts := strings.Split(childPath, "/")
		name := parts[0]
		for i, p := range parts {
			if i == 0 {
				continue
			}
			name += strings.Title(p)
		}

		if pkgGen == "" {
			pkgGen = "internal"
		}
		obj := &FileObject{
			Name:        utils.FirstLetterToLow(name),
			PackageName: pkgGen,
			HttpPath:    path.Join(basePath, childPath),
			Path:        filePath,
			Method:      method,
		}
		docPath := &DocPath{
			Path:   obj.HttpPath,
			Method: obj.Method,
		}
		s.Source = append(s.Source, obj)
		s.Docs.Routes = append(s.Docs.Routes, docPath)

		if route == nil {
			obj.ResponseParam = &Param{
				Type: "interface{}",
			}
			return
		}
		value := reflect.ValueOf(route)
		typ := value.Type()

		ctx := typ.In(0)
		obj.Context = ctx.String()

		if typ.NumIn() == 2 {
			request := typ.In(1)

			docPath.Request = valueOfType(request)
			obj.Packages = append(obj.Packages, request.PkgPath())
			obj.RequestParam = &Param{
				Package: request.PkgPath(),
				Type:    request.String(),
			}
		} else if typ.NumIn() != 1 ||
			!strings.HasPrefix(typ.In(0).String(), "bgen.") {
			panic("unsupported request: '" + typ.Name() + "'")
		}

		if typ.NumOut() != 2 && typ.Out(1).String() != "serviceUtils.Error" {
			panic("unsupported response: '" + typ.Name() + "'")
		}

		response := typ.Out(0)
		docPath.Response = valueOfType(response)
		obj.ResponseParam = &Param{
			Package: response.PkgPath(),
			Type:    response.String(),
		}

		docPath.Middlewares = s.commonFiles.Middlewares[method+":"+docPath.Path]
	})

	config.RangeChildren(func(childPath string, element serviceUtils.ConfigurableService) {
		s.Walk(path.Join(basePath, childPath), element.Router())
	})
}

func (s *Structure) CreateAll() {
	basePkg := path.Join(s.Package, DIR_NAME, "internal")

	for _, file := range s.Source {
		fullPackage := path.Join(basePkg, file.Path)
		splitted := strings.Split(file.HttpPath, "/")
		pkg := ""
		for i, s := range splitted {
			if s == "" {
				i = i - 1
				continue
			}
			if i == len(splitted)-1 {
				break
			}
			if i == 0 {
				pkg = s
			} else {
				pkg = pkg + strings.Title(s)
			}
		}
		if pkg == "" {
			pkg = "internal"
		}

		role := ""
		if rl, ok := s.commonFiles.RoleMiddlewares[file.Method+":"+file.HttpPath]; ok {
			role = rl
		}

		var ms []string
		for prefix, val := range s.commonFiles.Middlewares {
			spl := strings.Split(prefix, ":")
			validMethod := true

			if len(spl) == 2 {
				prefix = splitted[1]
				validMethod = file.Method == splitted[0]
			}

			if validMethod && strings.HasPrefix(file.HttpPath, prefix) {
				ms = append(ms, val...)
			}
		}

		s.commonFiles.Imports[pkg] = fullPackage
		s.commonFiles.Paths[file.HttpPath+file.Method] = &PathObject{
			Package:    pkg,
			Path:       file.HttpPath,
			Method:     file.Method,
			Function:   file.Method + "_" + file.Name,
			Wrapper:    file.Method + "_" + file.Name + "Handler",
			Role:       role,
			Middleware: ms,
		}

		pt := path.Join(gopathSrc, fullPackage)
		if err := os.MkdirAll(pt, os.ModePerm); err != nil {
			panic(err)
		}

		f, err := os.Create(path.Join(pt, fmt.Sprintf("%s:%s.go", strings.ToLower(file.Method), file.Name)))
		if err != nil {
			panic(err)
		}

		if err := file.Save(f); err != nil {
			panic(err)
		}

		raw := golang.GoRawFile{
			FullPackage: fullPackage,
			Name:        file.Name,
			Method:      file.Method,
		}

		if err = raw.Refactor(); err != nil {
			panic(err)
		}
	}

	// initializer file
	initFile := &golang.GoRawFile{
		Name:        "common",
		FullPackage: path.Join(s.Package, DIR_NAME),
	}

	fl, err := os.Create(initFile.Filepath())
	if err != nil {
		panic(err)
	}

	if err = s.commonFiles.Save(fl); err != nil {
		panic(err)
	}
	fmt.Println("Saved common file to " + initFile.Filepath())

	if err := initFile.Refactor(); err != nil {
		println(err)
	}
}

func makeGen(arr []string) []string {
	for i := 0; i < len(arr); i++ {
		if arr[i] == "" {
			continue
		}
		arr[i] = arr[i] + "Gen"
	}
	return arr
}
