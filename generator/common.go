package generator

import (
	"bitbucket.org/hasdfa/bgen/pkg/middlewareUtils"
	"bitbucket.org/hasdfa/bgen/pkg/serviceUtils"
	"os"
	"path"
	"reflect"
)

const (
	DOC_FILENAME = "rest.json"
	DIR_NAME     = ".gen"
)

var (
	gopath    = os.Getenv("GOPATH")
	gopathSrc = path.Join(gopath, "src") + "/"
)

func Generate(pkg string, controller serviceUtils.ConfigurableService, middlewares middlewareUtils.MiddlewareDictionary) {
	value := reflect.ValueOf(controller)

	s := &Structure{
		Package: pkg,
		Docs: &Documentation{
			Main: &DocMain{
				Project: pkg,
			},
			Middlewares: make(map[string]*DocMiddle),
		},

		commonFiles: &CommonFile{
			ControllerType:  value.Type().String(),
			Imports:         map[string]string{},
			Middlewares:     map[string][]string{},
			RoleMiddlewares: map[string]string{},
			Paths:           map[string]*PathObject{},
		},
		middlewares:     middlewares,
		rolesMiddleware: middlewareUtils.RolesFrom(middlewares),
	}
	s.Walk("/", controller.Router())

	Clear(pkg)
	s.CreateAll()
	s.MakeDocs()
}

func Clear(pkg string) {
	var err error
	var fileObj *os.File

	file := path.Join(gopathSrc, pkg)
	clearDir(file)

	if err = os.Mkdir(path.Join(file, DIR_NAME), os.ModePerm); err != nil {
		panic(err)
	}
	if fileObj, err = os.Create(path.Join(file, DIR_NAME, "empty.go")); err != nil {
		panic(err)
	}
	defer fileObj.Close()
	if _, err = fileObj.Write([]byte(`package _gen

import (
	"bitbucket.org/hasdfa/bgen/pkg/serverUtils"
	"github.com/buaazp/fasthttprouter"
	"github.com/kataras/iris"
	"github.com/kataras/muxie"
)

func InitStdRouter(r *muxie.Mux, s *serverUtils.BaseServer) {
	serverUtils.GenInitStdRouter(r, s)
}

func InitIrisRouter(r *iris.Application, s *serverUtils.BaseServer) {
	serverUtils.GenInitIrisRouter(r, s)
}

func InitFasthttpRouter(r *fasthttprouter.Router, s *serverUtils.BaseServer) {
	serverUtils.GenInitFasthttpRouter(r, s)
}
`)); err != nil {
		panic(err)
	}
}

//func projectPath() string {
//	for i := 0; i < 100; i = i + 1 {
//		_, file, _, _ := runtime.Caller(i)
//		if strings.HasSuffix(file, "main.go") {
//			return strings.TrimSuffix(file, "main.go")
//		}
//	}
//	return ""
//}

func clearDir(p string) {
	if err := os.RemoveAll(path.Join(p, DIR_NAME)); err != nil {
		panic(err)
	}
}
