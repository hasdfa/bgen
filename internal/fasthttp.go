package internal

import (
	"bitbucket.org/hasdfa/bgen/pkg/dbUtils"
	"bitbucket.org/hasdfa/bgen/pkg/httpUtils"
	"bitbucket.org/hasdfa/bgen/pkg/serviceUtils"
	"github.com/valyala/fasthttp"
	"gopkg.in/go-playground/validator.v9"
	"net/http"
	"sync"
	"sync/atomic"
	"unsafe"
)

type (
	FasthttpContextImpl struct {
		Context *fasthttp.RequestCtx

		Status         int
		RequestBody    unsafe.Pointer
		ResponseBody   interface{}
		RawContentType string
		Pooled         bool

		Repo  dbUtils.DatabaseRepository
		Valid *validator.Validate

		Header http.Header
	}
)

var (
	authMap = &sync.Map{}
)

func (c *FasthttpContextImpl) GetHeader(key string) string {
	return string(c.Context.Request.Header.Peek(key))
}

func (c *FasthttpContextImpl) GetQuery(key string) string {
	return ""
}

func (c *FasthttpContextImpl) GetPathParam(key string) string {
	return ""
}

func (c *FasthttpContextImpl) ContentType() string {
	return string(c.Context.Request.Header.ContentType())
}

func (c *FasthttpContextImpl) Request() *http.Request {
	return nil
}

func (c *FasthttpContextImpl) ParseBody(i interface{}) (err error) {
	if c.RequestBody != nil {
		p := unsafe.Pointer(&i)
		atomic.CompareAndSwapPointer(&p, p, c.RequestBody)
		return
	}

	contentType := string(c.Context.Request.Header.ContentType())
	if contentType == "" {
		contentType = httpUtils.MIMEApplicationJSON
	}

	bin := c.Context.Request.Body()

	contentType, err = httpUtils.Unmarshal(bin, contentType, i)
	if err != nil {
		return
	}
	if err = c.Valid.Struct(i); err != nil {
		return
	}

	c.RequestBody = unsafe.Pointer(&i)
	return
}

func (c *FasthttpContextImpl) SetResponseHeader(key, value string) {
	c.Header.Set(key, value)
}

func (c *FasthttpContextImpl) RemoveResponseHeader(key string) {
	c.Header.Del(key)
}

func (c *FasthttpContextImpl) SetStatus(statusCode int) {
	c.Status = statusCode
}

func (c *FasthttpContextImpl) Response(i interface{}) {
	c.ResponseBody = i
}

func (c *FasthttpContextImpl) ResponseCode(statusCode int, i interface{}) {
	c.SetStatus(statusCode)
	c.Response(i)
}

func (c *FasthttpContextImpl) ServiceError(err serviceUtils.Error) {
	c.Status = err.Status()
	c.ResponseBody = err
}

func (c *FasthttpContextImpl) PushResponse() {
	if c.Pooled {
		panic("already polled")
	}
	defer func() {
		err := recover()
		if err == nil {
			c.Pooled = true
		} else {
			panic(err)
		}
	}()

	contentType := c.ContentType()
	if contentType == "" {
		contentType = c.RawContentType
	}

	if c.ResponseBody == nil {
		c.ResponseBody = serviceUtils.NewServiceErrorCode(c.Status)
	}

	content, contentType := httpUtils.MarshalContent(c.ResponseBody, contentType)

	c.Context.Response.SetStatusCode(c.Status)
	c.Context.Response.Header.Set(httpUtils.HeaderContentType, contentType)
	c.Context.Response.Header.Set(httpUtils.HeaderXContentTypeOptions, "nosniff")
	if _, err := c.Context.Response.BodyWriter().Write(content); err != nil {
		panic(err)
	}
}

func (c *FasthttpContextImpl) Repository() dbUtils.DatabaseRepository {
	return c.Repo
}

func (c *FasthttpContextImpl) IsPushed() bool {
	return c.Pooled
}

func (c *FasthttpContextImpl) SetAuthenticated(i interface{}) {
	authMap.Store(c.Context.ID(), i)
}

func (c *FasthttpContextImpl) IsAuthenticated() bool {
	_, ok := authMap.Load(c.Context.ID())
	return ok
}

func (c *FasthttpContextImpl) GetAuthenticated() interface{} {
	v, ok := authMap.Load(c.Context.ID())
	if !ok {
		return nil
	}
	return v
}
