package internal

import (
	"bitbucket.org/hasdfa/bgen/pkg/dbUtils"
	"bitbucket.org/hasdfa/bgen/pkg/httpUtils"
	"bitbucket.org/hasdfa/bgen/pkg/serviceUtils"
	"github.com/kataras/iris/context"
	"gopkg.in/go-playground/validator.v9"
	"net/http"
	"sync/atomic"
	"unsafe"
)

type (
	IrisContextImpl struct {
		Context context.Context

		Status         int
		RequestBody    unsafe.Pointer
		ResponseBody   interface{}
		RawContentType string
		Pooled         bool

		Repo  dbUtils.DatabaseRepository
		Valid *validator.Validate
	}
)

func (c *IrisContextImpl) GetQuery(key string) string {
	return c.Context.FormValue(key)
}

func (c *IrisContextImpl) GetPathParam(key string) string {
	return c.Context.Params().Get(key)
}

func (c *IrisContextImpl) Request() *http.Request {
	return c.Context.Request()
}

func (c *IrisContextImpl) GetHeader(key string) string {
	return c.Context.GetHeader(key)
}

func (c *IrisContextImpl) ContentType() string {
	return c.Context.Request().Header.Get(httpUtils.HeaderContentType)
}

func (c *IrisContextImpl) ParseBody(i interface{}) (err error) {
	if c.RequestBody != nil {
		p := unsafe.Pointer(&i)
		atomic.CompareAndSwapPointer(&p, p, c.RequestBody)
		return
	}

	contentType, err := httpUtils.UnmarshalRequest(c.Context.Request(), i)
	if err != nil {
		return
	}
	if err = c.Valid.Struct(i); err != nil {
		return
	}
	if contentType != "" {
		c.RawContentType = contentType
	}

	c.RequestBody = unsafe.Pointer(&i)
	return
}

func (c *IrisContextImpl) SetResponseHeader(key, value string) {
	c.Context.ResponseWriter().Header().Set(key, value)
}

func (c *IrisContextImpl) RemoveResponseHeader(key string) {
	c.Context.ResponseWriter().Header().Del(key)
}

func (c *IrisContextImpl) SetStatus(statusCode int) {
	c.Status = statusCode
}

func (c *IrisContextImpl) Response(i interface{}) {
	c.ResponseBody = i
}

func (c *IrisContextImpl) ResponseCode(statusCode int, i interface{}) {
	c.SetStatus(statusCode)
	c.Response(i)
}

func (c *IrisContextImpl) ServiceError(err serviceUtils.Error) {
	c.Status = err.Status()
	c.ResponseBody = err
}

func (c *IrisContextImpl) PushResponse() {
	if c.Pooled {
		panic("already polled")
	}
	defer func() {
		err := recover()
		if err == nil {
			c.Pooled = true
		} else {
			panic(err)
		}
	}()

	contentType := c.Context.ResponseWriter().Header().Get(httpUtils.HeaderContentType)
	if contentType == "" {
		contentType = c.RawContentType
	}

	if c.ResponseBody != nil {
		httpUtils.Write(c.Context.ResponseWriter(), contentType, c.ResponseBody, c.Status)
	} else {
		c.Context.ResponseWriter().WriteHeader(http.StatusNoContent)
	}
}

func (c *IrisContextImpl) Repository() dbUtils.DatabaseRepository {
	return c.Repo
}

func (c *IrisContextImpl) IsPushed() bool {
	return c.Pooled
}

func (c *IrisContextImpl) SetAuthenticated(i interface{}) {
	c.Context.Values().Set("auth", i)
}

func (c *IrisContextImpl) IsAuthenticated() bool {
	return c.GetAuthenticated() != nil
}

func (c *IrisContextImpl) GetAuthenticated() interface{} {
	return c.Context.Values().Get("auth")
}
