package internal

import (
	"bitbucket.org/hasdfa/bgen/pkg/dbUtils"
	"bitbucket.org/hasdfa/bgen/pkg/httpUtils"
	"bitbucket.org/hasdfa/bgen/pkg/serviceUtils"
	"context"
	"gopkg.in/go-playground/validator.v9"
	"net/http"
	"sync/atomic"
	"unsafe"
)

type (
	ContextImpl struct {
		HttpRequest *http.Request
		Writer      http.ResponseWriter

		Status         int
		RequestBody    unsafe.Pointer
		ResponseBody   interface{}
		RawContentType string
		Pooled         bool

		Repo  dbUtils.DatabaseRepository
		Valid *validator.Validate

		Headers http.Header
	}
)

func (c *ContextImpl) GetHeader(key string) string {
	return c.HttpRequest.Header.Get(key)
}

func (c *ContextImpl) GetQuery(key string) string {
	return c.HttpRequest.FormValue(key)
}

func (c *ContextImpl) GetPathParam(key string) string {
	return ""
}

func (c *ContextImpl) Request() *http.Request {
	return c.HttpRequest
}

func (c *ContextImpl) ContentType() string {
	return c.HttpRequest.Header.Get(httpUtils.HeaderContentType)
}

func (c *ContextImpl) ParseBody(i interface{}) (err error) {
	if c.RequestBody != nil {
		p := unsafe.Pointer(&i)
		atomic.CompareAndSwapPointer(&p, p, c.RequestBody)
		return
	}

	contentType, err := httpUtils.UnmarshalRequest(c.HttpRequest, i)
	if err != nil {
		return
	}
	if err = c.Valid.Struct(i); err != nil {
		return
	}
	if contentType != "" {
		c.RawContentType = contentType
	}

	c.RequestBody = unsafe.Pointer(&i)
	return
}

func (c *ContextImpl) SetResponseHeader(key, value string) {
	c.Headers.Set(key, value)
}

func (c *ContextImpl) RemoveResponseHeader(key string) {
	c.Writer.Header().Del(key)
}

func (c *ContextImpl) SetStatus(statusCode int) {
	c.Status = statusCode
}

func (c *ContextImpl) Response(i interface{}) {
	c.ResponseBody = i
}

func (c *ContextImpl) ResponseCode(statusCode int, i interface{}) {
	c.SetStatus(statusCode)
	c.Response(i)
}

func (c *ContextImpl) ServiceError(err serviceUtils.Error) {
	c.Status = err.Status()
	c.ResponseBody = err
}

func (c *ContextImpl) PushResponse() {
	if c.Pooled {
		panic("already polled")
	}
	defer func() {
		err := recover()
		if err == nil {
			c.Pooled = true
		} else {
			panic(err)
		}
	}()

	contentType := c.ContentType()
	if contentType == "" {
		contentType = c.RawContentType
	}

	if c.ResponseBody == nil {
		httpUtils.Write(c.Writer, contentType, c.ResponseBody, c.Status)
	} else {
		c.Writer.WriteHeader(http.StatusNoContent)
	}
}

func (c *ContextImpl) Repository() dbUtils.DatabaseRepository {
	return c.Repo
}

func (c *ContextImpl) IsPushed() bool {
	return c.Pooled
}

func (c *ContextImpl) SetAuthenticated(i interface{}) {
	c.HttpRequest = c.HttpRequest.WithContext(context.WithValue(c.HttpRequest.Context(), "auth", i))
}

func (c *ContextImpl) IsAuthenticated() bool {
	return c.GetAuthenticated() != nil
}

func (c *ContextImpl) GetAuthenticated() interface{} {
	return c.HttpRequest.Context().Value("auth")
}
