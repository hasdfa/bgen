package bgen

import (
	"bitbucket.org/hasdfa/bgen/pkg/dbUtils"
	"bitbucket.org/hasdfa/bgen/pkg/serviceUtils"
	"net/http"
)

type (
	FullContext interface {
		GetHeader(key string) string
		GetQuery(key string) string
		GetPathParam(key string) string

		Request() *http.Request
		ContentType() string
		ParseBody(i interface{}) error

		Response(i interface{})
		SetStatus(statusCode int)
		ResponseCode(statusCode int, i interface{})
		ServiceError(err serviceUtils.Error)

		PushResponse()
		IsPushed() bool

		SetResponseHeader(key, value string)
		RemoveResponseHeader(key string)

		SetAuthenticated(i interface{})
		IsAuthenticated() bool
		GetAuthenticated() interface{}

		Repository() dbUtils.DatabaseRepository
	}
)
