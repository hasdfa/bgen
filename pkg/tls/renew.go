package tls

import (
	"crypto/tls"
	"golang.org/x/crypto/acme/autocert"
)

type (
	SSLCertificatesService interface {
		Certificate() getCertificateFunc
		TlsConfig() *tls.Config
	}

	renewService struct {
		manager autocert.Manager
	}

	getCertificateFunc func(*tls.ClientHelloInfo) (*tls.Certificate, error)
)

func NewCertificatesService(hosts ...string) SSLCertificatesService {
	return &renewService{
		manager: autocert.Manager{
			Prompt:     autocert.AcceptTOS,
			HostPolicy: autocert.HostWhitelist(hosts...),
			Cache:      autocert.DirCache("backgen-ssl"),
		},
	}
}

func (s *renewService) Certificate() getCertificateFunc {
	return s.manager.GetCertificate
}

func (s *renewService) TlsConfig() *tls.Config {
	return &tls.Config{
		GetCertificate: s.Certificate(),
		NextProtos:     []string{"h2", "http/1.1"}, // Enable HTTP/2
	}
}
