package utils

import (
	"bitbucket.org/hasdfa/bgen"
)

func GetFromHeader(path string, f func(string) string) func(ctx bgen.Context) string {
	return func(ctx bgen.Context) string {
		return ctx.GetHeader(path)
	}
}

func GetFromQuery(path string) func(ctx bgen.Context) string {
	return func(ctx bgen.Context) string {
		return ctx.GetQuery(path)
	}
}

func GetFromPath(path string) func(ctx bgen.Context) string {
	return func(ctx bgen.Context) string {
		return ctx.GetPathParam(path)
	}
}
