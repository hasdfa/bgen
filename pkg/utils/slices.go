package utils

type StringSet struct {
	mp map[string]interface{}
}

func NewStringSet() *StringSet {
	return &StringSet{
		mp: map[string]interface{}{},
	}
}

var (
	nonNil = struct {}{}
)

func (set *StringSet) Range(fn func(string)) {
	for k, _ := range set.mp {
		fn(k)
	}
}

func (set *StringSet) Append(elements ...string) {
	for _, e := range elements {
		set.mp[e] = nil
	}
}

func (set *StringSet) Remove(elements ...string) {
	for _, e := range elements {
		delete(set.mp, e)
	}
}

func (set *StringSet) Array() []string {
	var arr []string
	for k, _ := range set.mp {
		arr = append(arr, k)
	}
	return arr
}