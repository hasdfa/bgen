package utils

import "net/http"

type CustomResponseWriter struct {
	Default http.ResponseWriter
	Status  int
}

func (w *CustomResponseWriter) Header() http.Header {
	return w.Default.Header()
}

func (w *CustomResponseWriter) Write(body []byte) (int, error) {
	return w.Default.Write(body)
}

func (w *CustomResponseWriter) WriteHeader(statusCode int) {
	w.Status = statusCode
	w.Default.WriteHeader(statusCode)
}
