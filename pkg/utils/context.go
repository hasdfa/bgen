package utils

import (
	"sync"
	"time"
)

type StdContext struct {
	DeadlineTime time.Time
	mp           *sync.Map
	Error        error
	done         chan struct{}
}

func NewEmptyStdContext() *StdContext {
	return &StdContext{
		DeadlineTime: time.Now().Add(time.Minute),
		mp:           &sync.Map{},
		done:         make(chan struct{}),
	}
}

func (ctx *StdContext) Deadline() (deadline time.Time, ok bool) {
	return ctx.DeadlineTime, ctx.DeadlineTime.After(time.Now())
}

func (ctx *StdContext) Done() <-chan struct{} {
	return ctx.done
}

func (ctx *StdContext) Err() error {
	return ctx.Error
}

func (ctx *StdContext) PutValue(key, value interface{}) {
	ctx.mp.Store(key, value)
}

func (ctx *StdContext) Exists(key interface{}) bool {
	_, ok := ctx.mp.Load(key)
	return ok
}

func (ctx *StdContext) Value(key interface{}) interface{} {
	v, ok := ctx.mp.Load(key)
	if !ok {
		return nil
	}
	return v
}
