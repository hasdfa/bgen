package filesUtils

import (
	"bitbucket.org/hasdfa/bgen"
	"bitbucket.org/hasdfa/bgen/pkg/serviceUtils"
	"bytes"
	"github.com/kataras/iris"
	irs "github.com/kataras/iris/context"
	"github.com/valyala/fasthttp"
	"net/http"
	"strings"
)

type (
	Handler func(ctx bgen.Context, path string) ([]byte, serviceUtils.Error)
)

func executeFileServe(ctx bgen.FullContext, path string, fn Handler) {
	response, err := fn(ctx, path)
	if err == nil {
		ctx.ResponseCode(http.StatusOK, response)
	} else {
		ctx.ServiceError(err)
	}

	ctx.PushResponse()
}

func StdFileServeHandler(base string, fn interface{}) http.Handler {
	handler := fn.(func(ctx bgen.Context, path string) ([]byte, serviceUtils.Error))
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := bgen.NewContext(w, r)
		if r.Method != http.MethodGet {
			ctx.ServiceError(serviceUtils.NewServiceErrorCode(http.StatusMethodNotAllowed))
			ctx.PushResponse()
		} else {
			executeFileServe(ctx,
				strings.TrimPrefix(r.URL.Path, base), handler,
			)
		}
	})
}

func IrisFileServeHandler(base string, fn interface{}) iris.Handler {
	handler := fn.(func(ctx bgen.Context, path string) ([]byte, serviceUtils.Error))
	return func(context irs.Context) {
		executeFileServe(
			bgen.NewContextFromIris(context),
			strings.TrimPrefix(context.Path(), base), handler,
		)
	}
}

func FasthttpFileServeHandler(base string, fn interface{}) fasthttp.RequestHandler {
	bts := []byte(base)
	handler := fn.(func(ctx bgen.Context, path string) ([]byte, serviceUtils.Error))
	return func(ctx *fasthttp.RequestCtx) {
		executeFileServe(bgen.NewFasthttpContext(ctx),
			string(bytes.TrimPrefix(ctx.Path(), bts)), handler,
		)
	}
}
