package middlewareUtils

import (
	"bitbucket.org/hasdfa/bgen"
	"bitbucket.org/hasdfa/bgen/pkg/serviceUtils"
	"fmt"
	"github.com/kataras/iris/context"
	"github.com/kataras/muxie"
	"github.com/valyala/fasthttp"
	"net/http"
	"strconv"
	"sync/atomic"
)

type (
	MiddlewareDictionary = map[string]*MiddlewareSource

	MiddlewareSource struct {
		Key         string
		Description string
		Middleware  FullMiddleware
		params      map[string]string

		Headers []*Param
		Queries []*Param
	}

	Param struct {
		Value       string `json:"value"`
		Description string `json:"description,omitempty"`
	}

	Handler func() serviceUtils.Error

	Middleware func(
		ctx bgen.Context,
		params map[string]string,
		next Handler,
	) Handler

	FullMiddleware func(
		ctx bgen.FullContext,
		params map[string]string,
		next Handler,
	) Handler
)

const (
	RoleKey = "role"
)

func Map(m []*MiddlewareSource) MiddlewareDictionary {
	dick := MiddlewareDictionary{}
	for _, k := range m {
		dick[k.Key] = k
	}
	return dick
}

func RolesFrom(m MiddlewareDictionary) *MiddlewareSource {
	if r, ok := m[RoleKey]; ok {
		return r
	}
	return nil
}

func (m *MiddlewareSource) WithKey(key string) *MiddlewareSource {
	m.Key = key
	return m
}

// Documentation

func (m *MiddlewareSource) WithDescription(desc string) *MiddlewareSource {
	m.Description = desc
	return m
}

func (m *MiddlewareSource) RequireHeader(header, description string) *MiddlewareSource {
	m.Headers = append(m.Headers,
		&Param{Value: header, Description: description},
	)
	return m
}

func (m *MiddlewareSource) RequireQuery(query, description string) *MiddlewareSource {
	m.Headers = append(m.Headers,
		&Param{Value: query, Description: description},
	)
	return m
}

func (m *MiddlewareSource) SetRole(role string) *MiddlewareSource {
	return &MiddlewareSource{
		Key:        m.Key,
		Middleware: m.Middleware,
		params: map[string]string{
			RoleKey: role,
		},
	}
}

func (m *MiddlewareSource) SetParams(params map[string]string) *MiddlewareSource {
	return &MiddlewareSource{
		Key:        m.Key,
		Middleware: m.Middleware,
		params:     params,
	}
}

func (m *MiddlewareSource) MakeFasthttp(h fasthttp.RequestHandler) fasthttp.RequestHandler {
	return func(ctx *fasthttp.RequestCtx) {
		c := bgen.NewFasthttpContext(ctx)
		if err := m.Middleware(c, m.params, func() serviceUtils.Error {
			h(ctx)
			return nil
		})(); err != nil {
			c.ServiceError(err)
			c.PushResponse()
		}
	}
}

func (m *MiddlewareSource) HandleMuxie(handler http.Handler) http.Handler {
	return m.MakeMuxie()(handler)
}

func (m *MiddlewareSource) MakeMuxie() muxie.Wrapper {
	return func(handler http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			c := bgen.NewContext(w, r)
			if err := m.Middleware(c, m.params, func() (err serviceUtils.Error) {
				defer func() {
					if rec := recover(); rec != nil {
						err = serviceUtils.NewServiceTextError(fmt.Sprint(rec))
					}
				}()

				handler.ServeHTTP(c.Writer, c.HttpRequest)
				return
			})(); err != nil {
				c.ServiceError(err)
				c.PushResponse()
			}
		})
	}
}

func (m *MiddlewareSource) HandleIris(handler context.Handler) context.Handler {
	return func(context context.Context) {
		ctx := bgen.NewContextFromIris(context)
		if err := m.Middleware(ctx, m.params, func() serviceUtils.Error {
			handler(context)
			return nil
		})(); err != nil {
			ctx.ServiceError(err)
			ctx.PushResponse()
		}
	}
}

var count uint64 = 0

func MiddlewareOf(m Middleware) *MiddlewareSource {
	return &MiddlewareSource{
		Key: "m" + strconv.Itoa(int(atomic.AddUint64(&count, 1))),
		Middleware: func(ctx bgen.FullContext, params map[string]string, next Handler) Handler {
			return m(ctx, params, next)
		},
		params: map[string]string{},
	}
}

func MiddlewareKey(key string, m Middleware) *MiddlewareSource {
	return &MiddlewareSource{
		Key: key,
		Middleware: func(ctx bgen.FullContext, params map[string]string, next Handler) Handler {
			return m(ctx, params, next)
		},
		params: map[string]string{},
	}
}

func FullMiddlewareOf(m FullMiddleware) *MiddlewareSource {
	return &MiddlewareSource{
		Key:        "m" + strconv.Itoa(int(atomic.AddUint64(&count, 1))),
		Middleware: m,
		params:     map[string]string{},
	}
}

func FullMiddlewareKey(key string, m FullMiddleware) *MiddlewareSource {
	return &MiddlewareSource{
		Key:        key,
		Middleware: m,
		params:     map[string]string{},
	}
}
