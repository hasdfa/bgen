package configUtils

type ServerPort struct {
	Value   uint16  `json:"value"`
	AutoSSL *SSL    `json:"autoSSL"`
	Tls     *Tls    `json:"tls"`
}

type SSL struct {
	Domains []string `json:"domains"`
}

type Tls struct {
	KeyFile  string `json:"keyFile"`
	CertFile string `json:"certFile"`
}
