package configUtils

type (
	RepositoriesSource map[string]*DBRepository
	PropertiesList     map[string]interface{}

	ServerConfiguration struct {
		Name         string             `json:"name"`
		Environment  *Env               `json:"env"`
		Http         *HttpConfig        `json:"http"`
		Ws           *WsConfig          `json:"ws"`
		Repositories RepositoriesSource `json:"repositories"`
		Properties   PropertiesList     `json:"properties"`
	}

	Env struct {
		Path string `json:"path"`
	}

	HttpConfig struct {
		Ports []*ServerPort `json:"ports"`
	}

	WsConfig struct {
		Port *ServerPort `json:"port"`
	}

	DBRepository struct {
		Provider    string                 `json:"provider"`
		URL         string                 `json:"url"`
		Connection  map[string]interface{} `json:"connection"`
		Credentials map[string]interface{} `json:"credentials"`
	}
)

func (c *ServerConfiguration) Has() bool {
	return c.HasHttp() || c.HasWS()
}

func (c *ServerConfiguration) HasHttp() bool {
	return c.Http != nil && len(c.Http.Ports) > 0
}

func (c *ServerConfiguration) HasWS() bool {
	return c.Ws != nil
}

func (c *ServerConfiguration) HasDatabase() bool {
	return c.Repositories != nil
}
