package configUtils

import (
	"encoding/json"
	"io/ioutil"
)

var (
	shared *ServerConfiguration
)

func ParseFile(file *string) (*ServerConfiguration, error) {
	bin, err := ioutil.ReadFile(*file)
	if err != nil {
		return nil, err
	}

	shared = &ServerConfiguration{}
	err = json.Unmarshal(bin, shared)
	return shared, err
}

func Shared() *ServerConfiguration {
	return shared
}
