package serviceUtils

import (
	"net/http"
	"strings"
)

type (
	multipleError struct {
		Strings []string `json:"message"`
		Code    int      `json:"status"`
	}
)

func (err *multipleError) Error() string {
	return "[" + strings.Join(err.Strings, ", ") + "]"
}

func (err *multipleError) Status() int {
	return err.Code
}

func NewMultipleISETextErrors(errs []string) Error {
	return NewMultipleTextErrors(http.StatusInternalServerError, errs...)
}
func NewMultipleBRTextErrors(errs []string) Error {
	return NewMultipleTextErrors(http.StatusBadRequest, errs...)
}

func NewMultipleISEErrors(errs ...error) Error {
	return NewMultipleErrors(http.StatusInternalServerError, errs...)
}

func NewMultipleBRErrors(errs ...error) Error {
	return NewMultipleErrors(http.StatusBadRequest, errs...)
}

func NewMultipleErrors(code int, errs ...error) Error {
	err := &multipleError{
		Code: code,
	}
	for _, e := range errs {
		err.Strings = append(err.Strings, e.Error())
	}
	return err
}

func NewMultipleTextErrors(code int, errs ...string) Error {
	return &multipleError{
		Strings: errs,
		Code:    code,
	}
}
