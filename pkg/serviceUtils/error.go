package serviceUtils

import (
	"fmt"
	"net/http"
	"strings"
)

type (
	Error interface {
		Error() string
		Status() int
	}

	internalError struct {
		String string `json:"message"`
		Code   int    `json:"code"`
	}
)

var (
	FuckingTeapot = NewServiceErrorCode(http.StatusTeapot)

	ErrorNotImplemented = NewServiceErrorCode(http.StatusNotImplemented)
	ErrorInternalServer = NewServiceErrorCode(http.StatusInternalServerError)
	ErrorNotFound       = NewServiceErrorCode(http.StatusNotFound)
	ErrorBadRequest     = NewServiceErrorCode(http.StatusBadRequest)
	ErrorUnauthorized   = NewServiceErrorCode(http.StatusUnauthorized)
	ErrorForbidden      = NewServiceErrorCode(http.StatusForbidden)
)

func (err *internalError) Error() string {
	return fmt.Sprintf(`{"error":"%s", "status": %d`, err.String, err.Code)
}

func (err *internalError) Status() int {
	return err.Code
}

func HandlerError(err error) Error {
	str := err.Error()
	if strings.Contains(str, "not found") {
		return NewServiceNFError(err)
	}
	return NewServiceISEError(err)
}

func NewServiceFullError(err error, code int) Error {
	return &internalError{
		String: err.Error(),
		Code:   code,
	}
}

func NewServiceFullTextError(err string, code int) Error {
	return &internalError{
		String: err,
		Code:   code,
	}
}

func NewServiceError(err error) Error {
	return &internalError{
		String: err.Error(),
		Code:   http.StatusInternalServerError,
	}
}

func NewServiceBRError(err error) Error {
	return &internalError{
		String: err.Error(),
		Code:   http.StatusBadRequest,
	}
}

func NewServiceNFError(err error) Error {
	return &internalError{
		String: err.Error(),
		Code:   http.StatusNotFound,
	}
}

func NewServiceISEError(err error) Error {
	return &internalError{
		String: err.Error(),
		Code:   http.StatusInternalServerError,
	}
}

func NewServiceTextError(err string) Error {
	return &internalError{
		String: err,
		Code:   http.StatusInternalServerError,
	}
}

func NewServiceBRTextError(err string) Error {
	return &internalError{
		String: err,
		Code:   http.StatusBadRequest,
	}
}

func NewServiceNFTextError(err string) Error {
	return &internalError{
		String: err,
		Code:   http.StatusNotFound,
	}
}

func NewServiceISETextError(err string) Error {
	return &internalError{
		String: err,
		Code:   http.StatusInternalServerError,
	}
}

func NewServiceErrorCode(code int) Error {
	return &internalError{
		String: http.StatusText(code),
		Code:   code,
	}
}
