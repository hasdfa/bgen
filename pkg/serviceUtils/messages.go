package serviceUtils

type (
	Message struct {
		Message string `json:"message"`
	}
)

var (
	MessageSuccess = &Message{"success"}
	MessageLoading = &Message{"loading"}
	MessageUpdated = &Message{"updated"}
)
