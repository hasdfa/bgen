package serviceUtils

import (
	"net/http"
)

type (
	Definition         map[string]interface{}
	ChildrenDefinition map[string]ConfigurableService

	ServiceConfiguration struct {
		GET          Definition
		PUT          Definition
		POST         Definition
		PATCH        Definition
		DELETE       Definition
		Children     ChildrenDefinition
		FileServings Definition

		Middlewares []string
	}

	SecureGroup struct {
		Role string
		Dest ConfigurableService
	}

	SecurePath struct {
		Dest interface{}
		Role string
	}

	MiddlewarePath struct {
		Dest          interface{}
		MiddlewareKey string
	}

	ConfigurableService interface {
		Router() *ServiceConfiguration
	}
)

func (c *SecureGroup) Router() *ServiceConfiguration {
	return c.Dest.Router()
}

func (c *ServiceConfiguration) FindHandlers(method, path string) interface{} {
	switch method {
	case http.MethodGet:
		if result, ok := c.GET[path]; ok {
			return result
		}
		break
	case http.MethodPut:
		if result, ok := c.PUT[path]; ok {
			return result
		}
		break
	case http.MethodPost:
		if result, ok := c.POST[path]; ok {
			return result
		}
		break
	case http.MethodPatch:
		if result, ok := c.PATCH[path]; ok {
			return result
		}
		break
	case http.MethodDelete:
		if result, ok := c.DELETE[path]; ok {
			return result
		}
		break
	default:
		panic("unsupported http method:" + method)
	}
	return nil
}

func (c *ServiceConfiguration) FindChild(path string) interface{} {
	if result, ok := c.Children[path]; ok {
		return result
	}
	return nil
}

func (c *ServiceConfiguration) FindFileServings(path string) interface{} {
	if result, ok := c.FileServings[path]; ok {
		return result
	}
	return nil
}

func (c *ServiceConfiguration) RangeHandlers(f func(method, path string, route interface{})) {
	for k, v := range c.GET {
		f(http.MethodGet, k, v)
	}
	for k, v := range c.PUT {
		f(http.MethodPut, k, v)
	}
	for k, v := range c.POST {
		f(http.MethodPost, k, v)
	}
	for k, v := range c.PATCH {
		f(http.MethodPatch, k, v)
	}
	for k, v := range c.DELETE {
		f(http.MethodDelete, k, v)
	}
}

func (c *ServiceConfiguration) RangeFileServings(f func(path string)) {
	for k := range c.FileServings {
		f(k)
	}
}

func (c *ServiceConfiguration) RangeChildren(f func(path string, element ConfigurableService)) {
	for k, v := range c.Children {
		f(k, v)
	}
}

func (c *ServiceConfiguration) RangeServices(f func(path string, element ConfigurableService)) {
	for k, v := range c.Children {
		f(k, v.(ConfigurableService))
	}
}

func (c *ServiceConfiguration) RangeMiddleware(f func(middleware string)) {
	for _, v := range c.Middlewares {
		f(v)
	}
}
