package wsUtils

import (
	"crypto/rand"
	"crypto/tls"
	"fmt"
	websockets "github.com/gobwas/ws"
	"log"
	"net"

	"bitbucket.org/hasdfa/bgen/pkg/configUtils"
	"github.com/pborman/uuid"
)

type (
	authorizeConnection func(conn *Connection) (string, error)

	Server struct {
		list          ConnectionsCollection
		Authorization authorizeConnection
		onRead        readHandler

		listener net.Listener
		config   *tls.Config
		upgrader *websockets.Upgrader
	}
)

func NewServer(onRead readHandler) *Server {
	return &Server{
		upgrader: &websockets.Upgrader{},
		list:     NewConnectionsCollection(),
		onRead:   onRead,
	}
}

func (s *Server) Start(config *configUtils.WsConfig) (err error) {
	portValue := fmt.Sprintf(":%d", config.Port.Value)

	if config.Port.Tls != nil {
		cert, err := tls.LoadX509KeyPair(
			config.Port.Tls.CertFile,
			config.Port.Tls.KeyFile,
		)
		if err != nil {
			return err
		}

		s.config = &tls.Config{Certificates: []tls.Certificate{cert}}
		s.config.Rand = rand.Reader

		s.listener, err = tls.Listen("tcp", portValue, s.config)
		if err != nil {
			return err
		}
	} else {
		s.listener, err = net.Listen("tcp", portValue)
		if err != nil {
			return err
		}
	}
	if s.config != nil {
		fmt.Println("Starterd websocket server with tls at " + portValue)
	} else {
		fmt.Println("Starterd websocket server at " + portValue)
	}

	for {
		conn, err := s.listener.Accept()
		if err != nil {
			return err
		}

		_, err = s.upgrader.Upgrade(conn)
		if err != nil {
			log.Printf("upgrade error: %s", err)
		}
		connection := NewConnection(conn, s.onRead)

		if s.Authorization != nil {
			connection.Id, err = s.Authorization(connection)
			if err != nil {
				return err
			}
		} else {
			connection.Id = uuid.New()
		}

		s.list.Put(connection)

		//s.list.Put()
		//s.connections = append(s.connections, wsUtils.NewConnection(conn, func(connection *wsUtils.Connection, packet wsUtils.Packet) {
		//	s.Route(s.bgen.Of(connection), &packet)
		//}))
	}
}

func (s *Server) LocalAddr() string {
	return fmt.Sprint(s.listener.Addr())
}

func (s *Server) Stop() error {
	return s.listener.Close()
}
