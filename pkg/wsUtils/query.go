package wsUtils

import "sync"

type Query interface {
	Set(key, value interface{})
	Remove(key interface{})

	Get(key interface{}) (interface{}, bool)

	GetString(key interface{}) (string, bool)
	GetStringArray(key interface{}) ([]string, bool)

	GetInt(key interface{}) (int, bool)
	GetIntArray(key interface{}) ([]int, bool)
}


type QueryResolver interface {
	Connections() []string
}

func NewQuery() Query {
	return &queryImpl{
		m: &sync.Map{},
	}
}

type queryImpl struct {
	m *sync.Map
}

func (obj *queryImpl) Set(key, value interface{}) {
	obj.m.Store(key, value)
}

func (obj *queryImpl) Remove(key interface{}) {
	obj.m.Delete(key)
}

func (obj *queryImpl) Get(key interface{}) (interface{}, bool) {
	return obj.m.Load(key)
}

func (obj *queryImpl) GetString(key interface{}) (string, bool) {
	if v, ok := obj.m.Load(key); ok {
		s, ok := v.(string)
		return s, ok
	}
	return "", false
}

func (obj *queryImpl) GetStringArray(key interface{}) ([]string, bool) {
	if v, ok := obj.m.Load(key); ok {
		s, ok := v.([]string)
		return s, ok
	}
	return nil, false
}

func (obj *queryImpl) GetInt(key interface{}) (int, bool) {
	if v, ok := obj.m.Load(key); ok {
		i, ok := v.(int)
		return i, ok
	}
	return 0, false
}

func (obj *queryImpl) GetIntArray(key interface{}) ([]int, bool) {
	if v, ok := obj.m.Load(key); ok {
		i, ok := v.([]int)
		return i, ok
	}
	return nil, false
}