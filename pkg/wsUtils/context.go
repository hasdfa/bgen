package wsUtils

type BaseContext struct {
	Connection  *Connection
	Resolver    QueryResolver
}

func (c *BaseContext) ReportError(err error) {
	c.Connection.handleError(err)
}

func (c *BaseContext) HandleError(err error) {
	if err != nil {
		c.ReportError(err)
	}
}