package wsUtils

type (
	// Packet represents application level data.
	Packet struct {
		Path    string     `json:"path"`
		Content PackedData `json:"content"`
	}

	Parseable interface {
		Unpack(*Packet)
	}

	PackedData map[string]interface{}
)

func (pd PackedData) String(key string) string {
	return pd[key].(string)
}

func (pd PackedData) Int(key string) int {
	return pd[key].(int)
}

func (pd PackedData) Float32(key string) float32 {
	return pd[key].(float32)
}

func (pd PackedData) Float64(key string) float64 {
	return pd[key].(float64)
}

func (pd PackedData) PackedData(key string) PackedData {
	return pd[key].(PackedData)
}