package wsUtils

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gobwas/ws"
	"io"
	"net"
	"sync"
	"sync/atomic"
	"time"
	"unsafe"
)

const (
	PacketPathError = "/__defined/error"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait
	pingPeriod = (pongWait * 8) / 10

	byteBufferSize   = 1
	smallBufferSize  = 32
	middleBufferSize = 128
	bigBufferSize    = 256

	errorTemplate = `{"path":"` + PacketPathError + `","content":"%s"}`
)

type (
	readHandler func(*Connection, Packet)
)

// Connection wraps user connection.
type Connection struct {
	Id       string
	IsOpened bool

	conn       net.Conn
	handleRead readHandler
	err        error

	packets *sync.Pool

	byteBuffer  *sync.Pool
	smallBuffer *sync.Pool
	midBuffer   *sync.Pool
	bigBuffer   *sync.Pool
}

func generatePool(size int) *sync.Pool {
	return &sync.Pool{
		New: func() interface{} {
			return make([]byte, size)
		},
	}
}

func NewConnection(conn net.Conn, read readHandler) *Connection {
	c := &Connection{
		conn:       conn,
		IsOpened:   true,
		handleRead: read,

		byteBuffer:  generatePool(byteBufferSize),
		smallBuffer: generatePool(smallBufferSize),
		midBuffer:   generatePool(middleBufferSize),
		bigBuffer:   generatePool(bigBufferSize),

		packets: &sync.Pool{
			New: func() interface{} {
				return Packet{}
			},
		},
	}
	go c.reader()

	return c
}

func (c *Connection) buffer(size int64, using func([]byte)) {
	var buf []byte
	if size <= byteBufferSize {
		buf = c.byteBuffer.Get().([]byte)
		defer c.byteBuffer.Put(buf)
	} else if size <= smallBufferSize {
		buf = c.smallBuffer.Get().([]byte)
		defer c.smallBuffer.Put(buf)
	} else if size <= middleBufferSize {
		buf = c.midBuffer.Get().([]byte)
		defer c.midBuffer.Put(buf)
	} else if size <= bigBufferSize {
		buf = c.bigBuffer.Get().([]byte)
		defer c.bigBuffer.Put(buf)
	} else {
		buf = make([]byte, size)
	}

	using(buf[:size])
}

func (c *Connection) reader() {
	defer func() {
		c.log("Closed")
		c.Close()
	}()

	for {
		header, err := ws.ReadHeader(c.conn)
		if err != nil {
			c.log(err.Error())
			return
		}
		if header.OpCode == ws.OpClose {
			return
		}
		if header.OpCode == ws.OpPing {
			c.log("ping " + c.conn.RemoteAddr().String())
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			c.conn.SetReadDeadline(time.Now().Add(pingPeriod))
			c.conn.SetDeadline(time.Now().Add(pongWait))
			c.pong()
			continue
		}

		c.buffer(header.Length, func(payload []byte) {
			_, err = io.ReadFull(c.conn, payload)
			if err != nil {
				c.handleError(err)
				return
			}
			if header.Masked {
				ws.Cipher(payload, header.Mask, 0)
			}

			packet := c.packets.Get().(Packet)
			defer c.packets.Put(packet)
			if err = json.Unmarshal(payload, &packet); err != nil {
				c.handleError(err)
				return
			}

			c.handleRead(c, packet)
		})
	}
}

func (c *Connection) LastError() error {
	return c.err
}

func (c *Connection) handleError(err error) {
	defer c.Close()

	c.err = err
	c.WriteB([]byte(fmt.Sprintf(errorTemplate, err.Error())))
}

func (c *Connection) Close() error {
	if !c.IsOpened {
		return errors.New("already closed")
	}
	return c.close()
}

func (c *Connection) CloseIfNeedIt() error {
	if c.IsOpened {
		return c.close()
	}
	return nil
}

func (c *Connection) deleteAndClose() error {
	if err := c.CloseIfNeedIt(); err != nil {
		return err
	}
	for {
		p := unsafe.Pointer(c)
		if p == nil {
			return errors.New("could not delete connection:" + c.Id)
		}
		if atomic.CompareAndSwapPointer(&p, p, nil) {
			return nil
		}
	}
}

func (c *Connection) close() (err error) {
	defer func() { c.IsOpened = false }()
	if err = c.conn.Close(); err != nil {
		c.err = err
	}
	return err
}

func (c *Connection) Write(p Packet) {
	js, err := json.Marshal(p)
	if err == nil {
		c.WriteB(js)
	} else {
		c.handleError(err)
	}
}

func (c *Connection) WriteP(p *Packet) {
	bin, err := json.Marshal(p)
	if err == nil {
		c.WriteB(bin)
	} else {
		c.handleError(err)
	}
}

func (c *Connection) WriteB(bytes []byte) {
	c.WriteF(ws.NewTextFrame(bytes))
}

func (c *Connection) WriteF(frame ws.Frame) {
	if err := ws.WriteFrame(c.conn, frame); err != nil {
		c.handleError(err)
	}
}

func (c *Connection) pong() {
	_, err := c.conn.Write(ws.CompiledPong)
	if err != nil {
		c.handleError(err)
	}
}

func (c *Connection) log(str string) {
	c.print("->", str)
}

func (c *Connection) print(splitter, str string) {
	fmt.Println("["+time.Now().Format(time.StampMilli)+"]", c.conn.RemoteAddr().String(), splitter, str)
}
