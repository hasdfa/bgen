package wsUtils

import (
	"errors"
	"github.com/pborman/uuid"
	"sync"
)

type ConnectionsCollection interface {
	Put(c *Connection)
	Get(id string) *Connection

	RemoveConnection(id string) error

	Query(q QueryResolver) (ConnectionsArray, []error)
	QueryIds(ids ...string) (ConnectionsArray, []error)
}

type cList struct {
	mp *sync.Map
}

func NewConnectionsCollection() ConnectionsCollection {
	return &cList{
		mp: &sync.Map{},
	}
}

func (obj *cList) Put(c *Connection) {
	obj.mp.Store(c.Id, c)
}

func (obj *cList) PutUnknown(c *Connection) string {
	c.Id = uuid.New()
	obj.Put(c)

	return c.Id
}

func (obj *cList) Get(id string) *Connection {
	if c, ok := obj.mp.Load(id); ok {
		return c.(*Connection)
	}
	return nil
}

func (obj *cList) RemoveConnection(id string) error {
	if c, ok := obj.mp.Load(id); ok {
		defer obj.mp.Delete(id)

		conn := c.(*Connection)
		return conn.CloseIfNeedIt()
	}

	return errors.New(id + " - was not found")
}

func (obj *cList) Query(q QueryResolver) (ConnectionsArray, []error) {
	return obj.QueryIds(q.Connections()...)
}

func (obj *cList) QueryIds(ids ...string) (ConnectionsArray, []error) {
	var arr []*Connection
	var errs []error

	for _, id := range ids {
		if conn := obj.Get(id); conn != nil {
			arr = append(arr, conn)
		} else {
			errs = append(errs, errors.New(id+" - was not found"))
		}
	}

	return &cArray{
		arr: arr,
	}, errs
}

type ConnectionsArray interface {
	SendPacket(p *Packet)
	SendBytes(b []byte)
	Iterate(fn func(*Connection))

	CloseAll() []error
}

type cArray struct {
	arr []*Connection
}

func (obj *cArray) SendPacket(p *Packet) {
	for _, conn := range obj.arr {
		conn.WriteP(p)
	}
}

func (obj *cArray) SendBytes(b []byte) {
	for _, conn := range obj.arr {
		conn.WriteB(b)
	}
}

func (obj *cArray) Iterate(fn func(*Connection)) {
	for _, conn := range obj.arr {
		fn(conn)
	}
}

func (obj *cArray) CloseAll() []error {
	var errs []error
	var err error

	for _, conn := range obj.arr {
		if err = conn.Close(); err != nil {
			errs = append(errs, err)
		}
	}

	return errs
}
