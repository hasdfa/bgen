package httpUtils

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"reflect"
	"strings"

	"github.com/golang/protobuf/proto"
	"github.com/vmihailenco/msgpack"
)

const (
	messageIsNotSameType = "%T is not a subtype of %s"
)

func MarshalContent(i interface{}, contentType string) ([]byte, string) {
	bytes, err := marshalContent(i, contentType)
	if err == nil {
		return bytes, contentType
	}

	return MarshalErrorContent(err, contentType)
}

func marshalContent(i interface{}, contentType string) ([]byte, error) {
	if bts, ok := i.([]byte); ok {
		return bts, nil
	}

	start := strings.TrimSpace(strings.Split(contentType, ";")[0])
	if start == MIMEApplicationJSON {
		return json.Marshal(i)
	} else if start == MIMEApplicationXML || start == MIMETextXML {
		v := reflect.ValueOf(i)

		if v.Kind() == reflect.Map {
			i = iToMap(v)
		}

		return xml.Marshal(i)
	} else if start == MIMEApplicationMsgpack {
		return msgpack.Marshal(i)
	} else if start == MIMEApplicationProtobuf {
		if v, ok := i.(proto.Message); ok {
			return proto.Marshal(v)
		}
		return nil, fmt.Errorf(messageIsNotSameType, i, "proto.Message")
	} else if start == MIMEMultipartForm {
		// TODO: implement
	}

	return []byte(fmt.Sprint(i)), nil
}

func MarshalErrorContent(err error, contentType string) ([]byte, string) {
	bytes, err := marshalContent(err.Error(), contentType)
	if err == nil {
		return bytes, contentType
	}

	return []byte(err.Error()), MIMETextPlain
}

type Response map[string]interface{}

func (d Response) MarshalXML(e *xml.Encoder, start xml.StartElement) (err error) {
	e.EncodeToken(start)
	for key, value := range d {
		name := xml.Name{Local: key}

		err = e.EncodeToken(xml.StartElement{Name: name})
		if err != nil {
			return err
		}

		err = parseXmlValue(e, value)
		if err != nil {
			return err
		}

		err = e.EncodeToken(xml.EndElement{Name: name})
		if err != nil {
			return err
		}
	}
	e.EncodeToken(xml.EndElement{Name: start.Name})

	// flush to ensure tokens are written
	if err = e.Flush(); err != nil {
		return err
	}

	return nil
}

func iToMap(v reflect.Value) Response {
	dest := Response{}

	for _, key := range v.MapKeys() {
		dest[fmt.Sprint(key)] = v.MapIndex(key).Interface()
	}
	return dest
}

var (
	difficultKinds = []reflect.Kind{
		reflect.Interface,
		reflect.Array,
		reflect.Chan,
		reflect.Func,
		reflect.Struct,
		reflect.Ptr,
		reflect.Slice,
		reflect.UnsafePointer,
	}
)

func isPrimitive(k reflect.Kind) bool {
	for _, v := range difficultKinds {
		if v == k {
			return false
		}
	}
	return true
}

func parseXmlValue(e *xml.Encoder, i interface{}) error {
	v := reflect.ValueOf(i)
	if isPrimitive(v.Kind()) {
		return e.EncodeToken(xml.CharData(fmt.Sprint(i)))
	} else if v.Kind() == reflect.Map {
		return e.Encode(iToMap(v))
	}
	return e.Encode(i)
}
