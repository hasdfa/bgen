package httpUtils

import (
	"encoding/json"
	"encoding/xml"
	"errors"
	"fmt"
	"github.com/golang/protobuf/proto"
	"github.com/vmihailenco/msgpack"
	"io/ioutil"
	"net/http"
	"strings"
)

const (
	parseTagHeader = "header"
	parseTagQuery  = "query"
	parseTagForm   = "form"
)

func UnmarshalRequest(r *http.Request, obj interface{}) (contentType string, err error) {
	contentType = r.Header.Get(HeaderContentType)
	if contentType == "" {
		contentType = MIMEApplicationJSONCharsetUTF8
	}

	if err = bindData(obj, r.Header, parseTagHeader); err != nil {
		return
	}
	if err = bindData(obj, r.URL.Query(), parseTagQuery); err != nil {
		return
	}

	if r.Method == http.MethodGet || r.Method == http.MethodDelete {
		return
	}
	if r.ContentLength == 0 {
		return contentType, errors.New("request body can't be empty")
	}

	if strings.HasPrefix(contentType, MIMEMultipartForm) {
		err := r.ParseForm()
		if err != nil {
			return contentType, err
		}
		if err = bindData(obj, r.Form, parseTagForm); err != nil {
			return contentType, err
		}
		return contentType, nil
	}

	bin, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()

	if err != nil {
		return contentType, err
	}
	return Unmarshal(bin, contentType, obj)
}

func Unmarshal(bin []byte, contentType string, obj interface{}) (string, error) {
	if strings.HasPrefix(contentType, MIMEApplicationJSON) {
		return contentType, json.Unmarshal(bin, obj)
	} else if strings.HasPrefix(contentType, MIMEApplicationXML) ||
		strings.HasPrefix(contentType, MIMETextXML) {
		return contentType, xml.Unmarshal(bin, obj)
	} else if strings.HasPrefix(contentType, MIMEApplicationMsgpack) {
		return contentType, msgpack.Unmarshal(bin, obj)
	} else if strings.HasPrefix(contentType, MIMEApplicationProtobuf) {
		if v, ok := obj.(proto.Message); ok {
			return contentType, proto.Unmarshal(bin, v)
		}
		return contentType, fmt.Errorf(messageIsNotSameType, obj, "proto.Message")
	}

	return contentType, errors.New("Unsupported content-type: " + contentType)
}
