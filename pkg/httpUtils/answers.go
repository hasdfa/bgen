package httpUtils

import (
	"net/http"
)

func WriteB(w http.ResponseWriter, contentType string, body []byte, code int) {
	w.Header().Set(HeaderContentType, contentType)
	w.Header().Set(HeaderXContentTypeOptions, "nosniff")
	w.WriteHeader(code)
	w.Write(body)
}

func Write(w http.ResponseWriter, contentType string, body interface{}, code int) {
	content, contentType := MarshalContent(body, contentType)
	WriteB(w, contentType, content, code)
}

func HttpError(w http.ResponseWriter, contentType, err string, code int) {
	if contentType == "" {
		contentType = MIMETextPlain
	}
	content, contentType := MarshalContent(
		Response{"message": err}, contentType,
	)
	WriteB(w, contentType, content, code)
}

func BadRequestError(w http.ResponseWriter, contentType, err string) {
	HttpError(w, contentType, err, http.StatusInternalServerError)
}

func InternalServerError(w http.ResponseWriter, contentType, err string) {
	HttpError(w, contentType, err, http.StatusInternalServerError)
}

func ForbiddenError(w http.ResponseWriter, contentType, err string) {
	HttpError(w, contentType, err, http.StatusForbidden)
}

func JsonHttpError(w http.ResponseWriter, err string, code int) {
	HttpError(w, MIMEApplicationJSONCharsetUTF8, err, code)
}

func JsonBadRequestError(w http.ResponseWriter, err string) {
	BadRequestError(w, MIMEApplicationJSONCharsetUTF8, err)
}

func JsonInternalServerError(w http.ResponseWriter, err string) {
	InternalServerError(w, MIMEApplicationJSONCharsetUTF8, err)
}

func JsonForbiddenError(w http.ResponseWriter, err string) {
	ForbiddenError(w, MIMEApplicationJSONCharsetUTF8, err)
}

func XMLHttpError(w http.ResponseWriter, err string, code int) {
	HttpError(w, MIMEApplicationXMLCharsetUTF8, err, code)
}

func XMLBadRequestError(w http.ResponseWriter, err string) {
	BadRequestError(w, MIMEApplicationXMLCharsetUTF8, err)
}

func XMLInternalServerError(w http.ResponseWriter, err string) {
	InternalServerError(w, MIMEApplicationXMLCharsetUTF8, err)
}

func XMLForbiddenError(w http.ResponseWriter, err string) {
	ForbiddenError(w, MIMEApplicationXMLCharsetUTF8, err)
}
