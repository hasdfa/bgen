package httpUtils

import (
	"bytes"
	"encoding/json"
	"encoding/xml"
	"io/ioutil"
	"net/http"
	"time"
)

type (
	RequestServer struct {
		BaseURL string
	}

	UserRequest struct {
		Path    string
		Method  string
		Headers http.Header
		Data    []byte
	}

	Result struct {
		Data       []byte
		RequestURI string

		Status     string
		StatusCode int
		Method     string

		UserAgent string
		Headers   *http.Header
	}
)

func NewRequestServer(base string) *RequestServer {
	return &RequestServer{
		BaseURL: base,
	}
}

func (r *RequestServer) Get(path string) *UserRequest {
	return &UserRequest{
		Path:   r.BaseURL + path,
		Method: http.MethodGet,
	}
}

func (r *RequestServer) Post(path string) *UserRequest {
	return &UserRequest{
		Path:   r.BaseURL + path,
		Method: http.MethodPost,
	}
}

func (r *RequestServer) Put(path string) *UserRequest {
	return &UserRequest{
		Path:   r.BaseURL + path,
		Method: http.MethodPut,
	}
}

func (r *RequestServer) Delete(path string) *UserRequest {
	return &UserRequest{
		Path:   r.BaseURL + path,
		Method: http.MethodDelete,
	}
}



func (r *UserRequest) WithData(data []byte) *UserRequest {
	r.Data = data
	return r
}

func (r *UserRequest) WithXml(i interface{}) *UserRequest {
	r.Data, _ = xml.Marshal(i)
	return r.WithHeader(
		HeaderContentType,
		MIMEApplicationXMLCharsetUTF8,
	)
}

func (r *UserRequest) WithJson(i interface{}) *UserRequest {
	r.Data, _ = json.Marshal(i)
	return r.WithHeader(
		HeaderContentType,
		MIMEApplicationJSONCharsetUTF8,
	)
}

func (r *UserRequest) WithHeader(key, value string) *UserRequest {
	r.Headers.Set(key, value)
	return r
}

func (r *UserRequest) Do() (*Result, error) {
	client := &http.Client{ Timeout: time.Minute }
	request, err := http.NewRequest(r.Method, r.Path, bytes.NewReader(r.Data))
	if err != nil {
		return nil, err
	}
	if r.Headers != nil {
		for k := range r.Headers {
			request.Header.Add(k, r.Headers.Get(k))
		}
	}

	resp, err := client.Do(request)
	if err != nil {
		return nil, err
	}

	resultData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	if err = resp.Body.Close(); err != nil {
		return nil, err
	}

	return &Result{
		Data: resultData,

		Status:     resp.Status,
		StatusCode: resp.StatusCode,

		RequestURI: resp.Request.RequestURI,
		Method:     resp.Request.Method,

		UserAgent: resp.Request.UserAgent(),
		Headers:   &resp.Header,
	}, nil
}

func (r *Result) Unmarshall(obj interface{}) error {
	_, err := Unmarshal(r.Data, r.Headers.Get(HeaderContentType), obj)
	return err
}
