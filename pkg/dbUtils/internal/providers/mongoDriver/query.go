package mongoDriver

import (
	"bitbucket.org/hasdfa/bgen/pkg/dbUtils/dbBase"
	"errors"
	"github.com/labstack/gommon/log"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type mongoFindQuery struct {
	collection *mongo.Collection
	options    options.FindOptions

	filter interface{}
	skip   int64
	limit  int64
	sort   interface{}
}

func (q *mongoFindQuery) Skip(n int) dbBase.Query            { q.skip = int64(n); return q }
func (q *mongoFindQuery) Limit(n int) dbBase.Query           { q.limit = int64(n); return q }
func (q *mongoFindQuery) Sort(fields ...string) dbBase.Query { q.sort = fields; return q }
func (q *mongoFindQuery) Select(selector []string) dbBase.Query {
	// TODO: find how to implement this
	log.Warn("MongoDriver does not supports select operation yet: ", selector)
	return q
}

func (q *mongoFindQuery) Count() (n int, err error) {
	opts := options.Count()
	if q.limit > 0 {
		opts.SetLimit(q.limit)
	}
	if q.skip > 0 {
		opts.SetSkip(q.skip)
	}

	n64, err := q.collection.CountDocuments(ctx(), q.filter, opts)
	if err != nil {
		return
	}
	return int(n64), nil
}

func (q *mongoFindQuery) Exists() (b bool, err error) {
	c, err := q.Count()
	if err != nil {
		return
	}
	return c > 0, nil
}

func (q *mongoFindQuery) One(result interface{}) error {
	opts := options.FindOne()
	if q.skip > 0 {
		opts.SetSkip(q.skip)
	}
	if q.sort != nil {
		opts.SetSort(q.sort)
	}

	found := q.collection.FindOne(ctx(), q.filter, opts)
	if found == nil {
		return errors.New("database error")
	}
	if err := found.Err(); err != nil {
		return err
	}

	if err := found.Decode(result); err != nil {
		return err
	}

	return nil
}

func (q *mongoFindQuery) All(result interface{}) (err error) {
	opts := options.Find()
	if q.skip > 0 {
		opts.SetSkip(q.skip)
	}
	if q.sort != nil {
		opts.SetSort(q.sort)
	}

	cursor, err := q.collection.Find(ctx(), q.filter, opts)
	if err != nil {
		return
	}

	err = cursor.Err()
	if err != nil {
		return
	}

	err = cursor.Current.Validate()
	if err != nil {
		return
	}

	err = cursor.All(ctx(), result)
	return
}
