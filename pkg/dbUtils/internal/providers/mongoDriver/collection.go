package mongoDriver

import (
	"bitbucket.org/hasdfa/bgen/pkg/dbUtils/dbBase"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"gopkg.in/mgo.v2"
	"reflect"
)

type mongoCollection struct {
	collection *mongo.Collection
}

func (c *mongoCollection) Find(args interface{}) dbBase.Query {
	return &mongoFindQuery{
		collection: c.collection,
		filter:     args,
	}
}

func (c *mongoCollection) FindId(query interface{}) dbBase.Query {
	return &mongoFindQuery{
		collection: c.collection,
		filter:     query,
	}
}

func (c *mongoCollection) Insert(docs ...interface{}) (err error) {
	res, err := c.collection.InsertMany(ctx(), docs)
	if err == nil && len(res.InsertedIDs) != len(docs) {
		err = fmt.Errorf("invalid insert count %d, should be %d", len(res.InsertedIDs), len(docs))
	}
	return
}

func (c *mongoCollection) Update(selector, update interface{}) (err error) {
	res, err := c.collection.UpdateOne(ctx(),
		selector, update,
		options.Update().SetUpsert(true),
	)
	if err == nil && res.UpsertedCount != 1 {
		err = fmt.Errorf("invalid update count %d, should be 1", res.UpsertedCount)
	}
	return
}

func (c *mongoCollection) UpdateId(id, update interface{}) (err error) {
	return c.Update(bson.M{"_id": id}, update)
}

func (c *mongoCollection) UpdateAll(selector, update interface{}) (err error) {
	res, err := c.collection.UpdateMany(ctx(),
		selector, update,
		options.Update().SetUpsert(true),
	)
	l := reflect.ValueOf(update).Len()
	if err == nil && int(res.UpsertedCount) != l {
		err = fmt.Errorf("invalid update count %d, should be %d", res.UpsertedCount, l)
	}
	return
}

func (c *mongoCollection) Remove(filter interface{}) (err error) {
	res, err := c.collection.DeleteMany(ctx(), filter)
	if err != nil {
		return
	}

	count := 1
	value := reflect.ValueOf(filter)
	if value.Kind() == reflect.Slice || value.Kind() == reflect.Array {
		count = value.Len()
	}

	if count != int(res.DeletedCount) {
		err = fmt.Errorf("invalid deleted count %d, should be %d", res.DeletedCount, count)
	}
	return
}

func (c *mongoCollection) RemoveId(id interface{}) (err error) {
	res, err := c.collection.DeleteOne(ctx(), dbBase.M{"_id": id})
	if err == nil && res.DeletedCount != 1 {
		err = fmt.Errorf("invalid deleted count: %d", res.DeletedCount)
	}
	return
}

func (c *mongoCollection) DropCollection() (err error) {
	return c.collection.Drop(ctx())
}

func (c *mongoCollection) Name() string {
	return c.collection.Name()
}

func (c *mongoCollection) DBRef(id interface{}) *dbBase.DBRef {
	return &dbBase.DBRef{
		Database:   c.collection.Database().Name(),
		Collection: c.collection.Name(),
		Id:         id,
	}
}

func (c *mongoCollection) Mgo() *mgo.Collection {
	panic("did not implemented")
}

func (c *mongoCollection) Mongo() *mongo.Collection {
	return c.collection
}
