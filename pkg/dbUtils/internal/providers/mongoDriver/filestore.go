package mongoDriver

import (
	"bitbucket.org/hasdfa/bgen/pkg/dbUtils/dbBase"
	"errors"
	"go.mongodb.org/mongo-driver/mongo/gridfs"
	"io/ioutil"
)

type (
	fileStore struct {
		bucket *gridfs.Bucket
	}

	uploadFileObj struct {
		name   string
		upload *gridfs.UploadStream
		bucket *gridfs.Bucket

		meta interface{}
	}

	downloadFileObj struct {
		name     string
		download *gridfs.DownloadStream
		bucket   *gridfs.Bucket

		meta interface{}
	}
)

func (s *fileStore) Create(name string) (dbBase.StoredFile, error) {
	stream, err := s.bucket.OpenUploadStream(name)
	if err != nil {
		return nil, err
	}
	stream.FileID = name

	return &uploadFileObj{
		name:   name,
		upload: stream,
		bucket: s.bucket,
	}, nil
}

func (s *fileStore) Open(name string) (dbBase.StoredFile, error) {
	stream, err := s.bucket.OpenDownloadStream(name)
	if err != nil {
		return nil, err
	}

	return &downloadFileObj{
		name:     name,
		download: stream,
		bucket:   s.bucket,
	}, nil
}

func (s *fileStore) Delete(name string) error {
	return s.bucket.Delete(name)
}

func (s *fileStore) Find(query, results interface{}) (err error) {
	res, err := s.bucket.Find(query)
	if err == nil {
		err = res.All(ctx(), results)
	}
	return
}

func (s *fileStore) FindAll(results interface{}) error {
	return s.Find(nil, results)
}

func (f *uploadFileObj) Name() string {
	return f.name
}

func (f *uploadFileObj) GetMeta(meta interface{}) error {
	return errors.New("metadata is deprecated")
}

func (f *uploadFileObj) SetName(name string) dbBase.StoredFile {
	if err := f.bucket.Rename(f.upload.FileID, name); err != nil {
		return nil
	}
	f.name = name
	return f
}

func (f *uploadFileObj) SetMetadata(metadata interface{}) dbBase.StoredFile {
	f.meta = metadata
	return f
}

func (f *uploadFileObj) Write(data []byte) error {
	_, err := f.upload.Write(data)
	return err
}

func (f *uploadFileObj) ReadAll() ([]byte, error) {
	return nil, errors.New("upload stream does not support reading")
}

func (f *uploadFileObj) Close() error {
	return f.upload.Close()
}

func (f *uploadFileObj) Abort() error {
	return f.upload.Abort()
}

func (f *downloadFileObj) Name() string {
	return f.name
}

func (f *downloadFileObj) GetMeta(meta interface{}) error {
	return errors.New("metadata is deprecated")
}

func (f *downloadFileObj) SetName(name string) dbBase.StoredFile {
	if err := f.bucket.Rename(f.download, name); err != nil {
		return nil
	}
	f.name = name
	return f
}

func (f *downloadFileObj) SetMetadata(metadata interface{}) dbBase.StoredFile {
	f.meta = metadata
	return f
}

func (f *downloadFileObj) Write(data []byte) error {
	return errors.New("download stream does not support witting")
}

func (f *downloadFileObj) ReadAll() ([]byte, error) {
	return ioutil.ReadAll(f.download)
}

func (f *downloadFileObj) Close() error {
	return f.download.Close()
}

func (f *downloadFileObj) Abort() error {
	return f.download.Close()
}
