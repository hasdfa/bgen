package mongoDriver

import (
	"bitbucket.org/hasdfa/bgen/pkg/dbUtils/dbBase"
	"context"
	"errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/gridfs"
	"go.mongodb.org/mongo-driver/mongo/options"
	"gopkg.in/mgo.v2"
	"time"
)

type mongoDatabase struct {
	database *mongo.Database
}

const (
	contextTimeout = time.Minute * 3
)

func ctx() (ctx context.Context) {
	ctx, _ = context.WithTimeout(
		context.Background(), contextTimeout,
	)
	return
}

func (db *mongoDatabase) Collections() ([]string, error) {
	return db.database.ListCollectionNames(ctx(), nil)
}

func (db *mongoDatabase) Collection(name string) dbBase.Collection {
	return &mongoCollection{
		collection: db.database.Collection(name),
	}
}

func (db *mongoDatabase) OneRef(ref *dbBase.DBRef, obj interface{}) error {
	if ref == nil {
		return errors.New("no such reference")
	}

	var source dbBase.Database = db
	if ref.Database != "" {
		source = &mongoDatabase{database: db.database.Client().Database(ref.Database)}
	}
	return source.Collection(ref.Collection).FindId(ref.Id).One(obj)
}

func (db *mongoDatabase) ManyRefs(ref *dbBase.DBMulRefs, obj interface{}) error {
	if ref == nil {
		return errors.New("no such references")
	}

	var source dbBase.Database = db
	if ref.Database != "" {
		source = &mongoDatabase{database: db.database.Client().Database(ref.Database)}
	}
	return source.Collection(ref.Collection).Find(bson.M{
		"_id": bson.M{
			"$in": ref.Ids,
		},
	}).All(obj)
}

func (db *mongoDatabase) Mgo() *mgo.Database {
	panic("did not implemented")
}

func (db *mongoDatabase) Mongo() *mongo.Database {
	return db.database
}

func (db *mongoDatabase) FileStore(name string) dbBase.FileStore {
	bucket, err := gridfs.NewBucket(db.database,
		options.GridFSBucket().SetName(name),
	)
	if err != nil {
		return nil
	}

	return &fileStore{bucket: bucket}
}
