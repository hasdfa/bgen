package mongoDriver

import (
	"bitbucket.org/hasdfa/bgen/pkg/configUtils"
	"bitbucket.org/hasdfa/bgen/pkg/dbUtils/dbBase"
	"crypto/tls"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"strings"
)

var (
	tlsConfig = &tls.Config{}
)

func Initializer(repo *configUtils.DBRepository) (db dbBase.Database, err error) {
	ops := options.Client()

	database := ""
	if repo.URL != "" {
		ops = ops.ApplyURI(repo.URL)

		pathes := strings.Split(repo.URL, "/")
		database = strings.Split(pathes[len(pathes)-1], "?")[0]
	} else {
		var temp interface{}
		var ok bool

		if temp, ok = repo.Connection["urls"]; ok {
			for _, k := range temp.([]interface{}) {
				ops.Hosts = append(ops.Hosts, k.(string))
			}
		}
		if temp, ok = repo.Connection["database"]; ok {
			database = temp.(string)
		}
		if temp, ok = repo.Connection["replicaSet"]; ok {
			ops = ops.SetReplicaSet(temp.(string))
		}
		//if temp, ok = repo.Connection["source"]; ok {
		//	info.Source = temp.(string)
		//}

		var username, password string
		if temp, ok = repo.Credentials["username"]; ok {
			username = temp.(string)
		}
		if temp, ok = repo.Credentials["password"]; ok {
			password = temp.(string)
		}

		if username != "" && password != "" {
			ops.Auth = &options.Credential{
				Username: username,
				Password: password,
			}
		}
	}

	client, err := mongo.NewClient(ops)
	if err != nil {
		return
	}

	return &mongoDatabase{
		database: client.Database(database),
	}, nil
}
