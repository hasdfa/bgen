package mgoDriver

import (
	"bitbucket.org/hasdfa/bgen/pkg/configUtils"
	"bitbucket.org/hasdfa/bgen/pkg/dbUtils/dbBase"
	"crypto/tls"
	"gopkg.in/mgo.v2"
	"net"
)

var (
	tlsConfig = &tls.Config{}
)

func Initializer(repo *configUtils.DBRepository) (dbBase.Database, error) {
	info := &mgo.DialInfo{
		DialServer: func(addr *mgo.ServerAddr) (net.Conn, error) {
			conn, err := tls.Dial("tcp", addr.String(), tlsConfig)
			return conn, err
		},
	}

	if repo.URL != "" {
		return NewMongoDatabase(repo.URL)
	}

	var temp interface{}
	var ok bool

	if temp, ok = repo.Connection["urls"]; ok {
		for _, k := range temp.([]interface{}) {
			info.Addrs = append(info.Addrs, k.(string))
		}
	}
	if temp, ok = repo.Connection["database"]; ok {
		info.Database = temp.(string)
	}
	if temp, ok = repo.Connection["replicaSet"]; ok {
		info.ReplicaSetName = temp.(string)
	}
	if temp, ok = repo.Connection["source"]; ok {
		info.Source = temp.(string)
	}

	if temp, ok = repo.Credentials["username"]; ok {
		info.Username = temp.(string)
	}
	if temp, ok = repo.Credentials["password"]; ok {
		info.Password = temp.(string)
	}

	return NewMongoDatabaseWithInfo(info)
}
