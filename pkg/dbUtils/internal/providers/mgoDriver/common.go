package mgoDriver

import (
	"bitbucket.org/hasdfa/bgen/pkg/dbUtils/dbBase"
	"gopkg.in/mgo.v2"
	"strings"
)

type tempDbase struct {
	db dbBase.Database
}

func NewMongoDatabaseWithInfo(info *mgo.DialInfo) (dbBase.Database, error) {
	sess, err := mgo.DialWithInfo(info)
	if err != nil {
		return nil, err
	}
	return newMongo(sess.DB(info.Database)), nil
}

func NewMongoDatabase(url string) (dbBase.Database, error) {
	sess, err := mgo.Dial(url)
	if err != nil {
		return nil, err
	}
	pathes := strings.Split(url, "/")
	db := strings.Split(pathes[len(pathes)-1], "?")[0]

	return newMongo(sess.DB(db)), nil
}

func newMongo(db *mgo.Database) dbBase.Database {
	return &mgoDatabase{db: db}
}
