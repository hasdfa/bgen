package mgoDriver

import (
	"bitbucket.org/hasdfa/bgen/pkg/dbUtils/dbBase"
	"gopkg.in/mgo.v2"
	"io/ioutil"
)

type (
	fileStore struct {
		grid *mgo.GridFS
	}

	fileObj struct {
		file *mgo.GridFile
	}
)

func (s *fileStore) Create(name string) (dbBase.StoredFile, error) {
	f, err := s.grid.Create(name)
	if err != nil {
		return nil, err
	}
	f.SetId(name)
	return &fileObj{file: f}, nil
}

func (s *fileStore) Open(name string) (dbBase.StoredFile, error) {
	f, err := s.grid.Open(name)
	if err != nil {
		return s.openId(name)
	}
	return &fileObj{file: f}, nil
}

func (s *fileStore) openId(name string) (dbBase.StoredFile, error) {
	f, err := s.grid.OpenId(name)
	if err != nil {
		return nil, err
	}
	return &fileObj{file: f}, nil
}

func (s *fileStore) Delete(name string) error {
	return s.grid.Remove(name)
}

func (s *fileStore) Find(query, results interface{}) error {
	return s.grid.Files.Find(query).All(results)
}

func (s *fileStore) FindAll(results interface{}) error {
	return s.Find(nil, results)
}

func (f *fileObj) Name() string {
	return f.file.Name()
}

func (f *fileObj) GetMeta(meta interface{}) error {
	return f.file.GetMeta(meta)
}

func (f *fileObj) SetName(name string) dbBase.StoredFile {
	f.file.SetName(name)
	return f
}

func (f *fileObj) SetMetadata(metadata interface{}) dbBase.StoredFile {
	f.file.SetMeta(metadata)
	return f
}

func (f *fileObj) Write(data []byte) error {
	_, err := f.file.Write(data)
	return err
}

func (f *fileObj) ReadAll() ([]byte, error) {
	return ioutil.ReadAll(f.file)
}

func (f *fileObj) Close() error {
	return f.file.Close()
}

func (f *fileObj) Abort() error {
	f.file.Abort()
	return nil
}
