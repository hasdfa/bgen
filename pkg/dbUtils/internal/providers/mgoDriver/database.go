package mgoDriver

import (
	"bitbucket.org/hasdfa/bgen/pkg/dbUtils/dbBase"
	"errors"
	"go.mongodb.org/mongo-driver/mongo"
	"gopkg.in/mgo.v2"
)

type mgoDatabase struct {
	db *mgo.Database
}

func (db *mgoDatabase) Mgo() *mgo.Database {
	return db.db
}

func (db *mgoDatabase) Mongo() *mongo.Database {
	panic("did not implemented")
}

func (db *mgoDatabase) OneRef(ref *dbBase.DBRef, obj interface{}) error {
	if ref == nil {
		return errors.New("no such reference")
	}

	var source dbBase.Database = db
	if ref.Database != "" {
		source = newMongo(db.db.Session.DB(ref.Database))
	}
	return source.Collection(ref.Collection).FindId(ref.Id).One(obj)
}

func (db *mgoDatabase) ManyRefs(ref *dbBase.DBMulRefs, obj interface{}) error {
	if ref == nil {
		return errors.New("no such references")
	}

	var source dbBase.Database = db
	if ref.Database != "" {
		source = newMongo(db.db.Session.DB(ref.Database))
	}
	return source.Collection(ref.Collection).Find(dbBase.M{
		"_id": dbBase.M{
			"$in": ref.Ids,
		},
	}).All(obj)
}

func (db *mgoDatabase) Collections() ([]string, error) {
	return db.db.CollectionNames()
}

func (db *mgoDatabase) Collection(name string) dbBase.Collection {
	return &mgoCollection{collection: db.db.C(name)}
}

func (db *mgoDatabase) FileStore(name string) dbBase.FileStore {
	return &fileStore{grid: db.db.GridFS(name)}
}
