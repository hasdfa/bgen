package mgoDriver

import (
	"bitbucket.org/hasdfa/bgen/pkg/dbUtils/dbBase"
	"go.mongodb.org/mongo-driver/mongo"
	"gopkg.in/mgo.v2"
)

type mgoCollection struct {
	collection *mgo.Collection
}

func (c *mgoCollection) Name() string {
	return c.collection.Name
}

func (c *mgoCollection) DBRef(id interface{}) *dbBase.DBRef {
	return &dbBase.DBRef{
		Collection: c.Name(),
		Id:         id,
	}
}

func (c *mgoCollection) Find(query interface{}) dbBase.Query {
	return &mgoQuery{c.collection.Find(query)}
}

func (c *mgoCollection) FindId(id interface{}) dbBase.Query {
	return &mgoQuery{c.collection.FindId(id)}
}

func (c *mgoCollection) Insert(docs ...interface{}) error {
	return c.collection.Insert(docs...)
}

func (c *mgoCollection) Update(selector interface{}, update interface{}) error {
	return c.collection.Update(selector, update)
}

func (c *mgoCollection) UpdateId(id interface{}, update interface{}) error {
	return c.collection.UpdateId(id, update)
}

func (c *mgoCollection) UpdateAll(selector interface{}, update interface{}) error {
	_, err := c.collection.UpdateAll(selector, update)
	return err
}

func (c *mgoCollection) Remove(selector interface{}) error {
	return c.collection.Remove(selector)
}

func (c *mgoCollection) RemoveId(id interface{}) error {
	return c.collection.RemoveId(id)
}

func (c *mgoCollection) DropCollection() error {
	return c.collection.DropCollection()
}

func (c *mgoCollection) Mgo() *mgo.Collection {
	return c.collection
}

func (c *mgoCollection) Mongo() *mongo.Collection {
	panic("did not implemented")
}
