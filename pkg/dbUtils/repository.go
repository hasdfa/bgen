package dbUtils

import (
	"bitbucket.org/hasdfa/bgen/pkg/configUtils"
	"bitbucket.org/hasdfa/bgen/pkg/dbUtils/dbBase"
	"bitbucket.org/hasdfa/bgen/pkg/dbUtils/internal/providers/mgoDriver"
	"bitbucket.org/hasdfa/bgen/pkg/dbUtils/internal/providers/mongoDriver"
	"fmt"
	"sync"
)

const (
	// supported providers Map
	mgoDriverProviderKey   = "mgoDriver"
	mongoDriverProviderKey = "mongoDriver"
)

var (
	supportedProviders = map[string]DatabaseInitializer{
		mgoDriverProviderKey:   mgoDriver.Initializer,
		mongoDriverProviderKey: mongoDriver.Initializer,
	}

	providerNames = func() (list []string) {
		for k := range supportedProviders {
			list = append(list, k)
		}
		return
	}()
)

type (
	DatabaseInitializer func(*configUtils.DBRepository) (dbBase.Database, error)

	DatabaseRepository interface {
		Init(configs configUtils.RepositoriesSource)

		Get(key string) dbBase.Database
		Set(key string, db dbBase.Database)
		Iterate(fn func(key string, value dbBase.Database))

		Main() dbBase.Database
		Default() dbBase.Database
	}

	dbRepository struct {
		mp sync.Map
	}
)

func NewDatabaseRepository(configs configUtils.RepositoriesSource) DatabaseRepository {
	repo := &dbRepository{}
	var db dbBase.Database
	var err error

	for name, cnfg := range configs {
		if provider, ok := supportedProviders[cnfg.Provider]; ok {
			if db, err = provider(cnfg); err == nil {
				repo.Set(name, db)
			} else {
				panic(err)
			}
		} else {
			panic(fmt.Sprintf("Unsupported provider: %s\n\tSupported providers: %s", cnfg.Provider, providerNames))
		}
	}
	return repo
}

func EmptyDatabaseRepository() DatabaseRepository {
	return &dbRepository{}
}

func (repo *dbRepository) Init(configs configUtils.RepositoriesSource) {
	var db dbBase.Database
	var err error

	for name, cnfg := range configs {
		if provider, ok := supportedProviders[cnfg.Provider]; ok {
			if db, err = provider(cnfg); err == nil {
				repo.Set(name, db)
			} else {
				panic(err)
			}
		} else {
			panic(fmt.Sprintf("Unsupported provider: %s\n\tSupported providers: %s", cnfg.Provider, providerNames))
		}
	}
}

func (repo *dbRepository) Get(key string) dbBase.Database {
	if db, ok := repo.mp.Load(key); ok {
		return db.(dbBase.Database)
	}
	return nil
}

func (repo *dbRepository) Set(key string, db dbBase.Database) {
	repo.mp.Store(key, db)
}

func (repo *dbRepository) Main() dbBase.Database {
	return repo.Get("main")
}

func (repo *dbRepository) Default() dbBase.Database {
	return repo.Get("default")
}

func (repo *dbRepository) Iterate(fn func(key string, value dbBase.Database)) {
	repo.mp.Range(func(key, value interface{}) bool {
		fn(key.(string), value.(dbBase.Database))
		return true
	})
}
