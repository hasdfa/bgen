package dbBase

import (
	"errors"
	"go.mongodb.org/mongo-driver/mongo"
	"gopkg.in/mgo.v2"
)

type tempDb struct{}

var (
	notImplemented = errors.New("not implemented")
)

func TempDb() Database {
	return &tempDb{}
}

func (*tempDb) Collection(name string) Collection {
	return &tempCollection{name: name}
}

func (*tempDb) Collections() ([]string, error)         { return make([]string, 0), notImplemented }
func (*tempDb) CreateCollection(string) error          { return notImplemented }
func (*tempDb) OneRef(*DBRef, interface{}) error       { return notImplemented }
func (*tempDb) ManyRefs(*DBMulRefs, interface{}) error { return notImplemented }
func (*tempDb) Mgo() *mgo.Database                     { return nil }
func (*tempDb) Mongo() *mongo.Database                 { return nil }
func (*tempDb) FileStore(string) FileStore             { return nil }

type tempCollection struct {
	name string
}

func (c *tempCollection) Name() string {
	return c.name
}
func (c *tempCollection) DBRef(id interface{}) *DBRef {
	return &DBRef{Collection: c.name, Id: id}
}

func (*tempCollection) Find(args interface{}) Query                           { return nil }
func (*tempCollection) FindId(query interface{}) Query                        { return nil }
func (*tempCollection) Insert(docs ...interface{}) error                      { return notImplemented }
func (*tempCollection) Update(selector interface{}, update interface{}) error { return notImplemented }
func (*tempCollection) UpdateId(id interface{}, update interface{}) error     { return notImplemented }
func (*tempCollection) UpdateAll(selector interface{}, update interface{}) error {
	return notImplemented
}
func (*tempCollection) Remove(selector interface{}) error { return notImplemented }
func (*tempCollection) RemoveId(id interface{}) error     { return notImplemented }
func (*tempCollection) DropCollection() error             { return notImplemented }
func (*tempCollection) Mgo() *mgo.Collection              { return nil }
func (*tempCollection) Mongo() *mongo.Collection          { return nil }
