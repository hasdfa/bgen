package dbBase

import (
	"go.mongodb.org/mongo-driver/mongo"
	"gopkg.in/mgo.v2"
)

type Collection interface {
	Find(args interface{}) Query
	FindId(query interface{}) Query

	Insert(docs ...interface{}) error

	Update(selector interface{}, update interface{}) error
	UpdateId(id interface{}, update interface{}) error
	UpdateAll(selector interface{}, update interface{}) error

	Remove(selector interface{}) error
	RemoveId(id interface{}) error

	DropCollection() error
	Name() string

	DBRef(id interface{}) *DBRef

	// For specific queries, like Pipe with aggregation framework
	Mgo() *mgo.Collection
	Mongo() *mongo.Collection
}
