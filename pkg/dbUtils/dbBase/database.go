package dbBase

import (
	"go.mongodb.org/mongo-driver/mongo"
	"gopkg.in/mgo.v2"
)

type Database interface {
	Collections() ([]string, error)
	Collection(name string) Collection

	OneRef(*DBRef, interface{}) error
	ManyRefs(*DBMulRefs, interface{}) error

	Mgo() *mgo.Database
	Mongo() *mongo.Database

	FileStore(name string) FileStore
}
