package dbBase

type (
	FileStore interface {
		Create(name string) (StoredFile, error)
		Open(name string) (StoredFile, error)
		Delete(name string) error

		Find(query, results interface{}) error
		FindAll(results interface{}) error
	}

	StoredFile interface {
		Name() string
		SetName(name string) StoredFile

		GetMeta(meta interface{}) error
		SetMetadata(metadata interface{}) StoredFile

		ReadAll() ([]byte, error)
		Write(data []byte) error

		Close() error
		Abort() error
	}
)
