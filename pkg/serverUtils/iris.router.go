package serverUtils

//func (s *IrisServer) parseRoutes(party iris.Party, conf *serviceUtils.ServiceConfiguration) {
//	conf.RangeMiddleware(func(middleware string) {
//		//if v, ok := s.DefinedMiddlewares[middleware]; ok {
//			// party.Use(v.MakeIris())
//			//fmt.Printf(`{ "type": "middleware", "path": "%s/*", "details": "%s" }`+"\n", party.GetRelPath(), middleware)
//		//} else {
//		//	panic("middleware '" + middleware + "' must be provided")
//		//}
//	})
//
//	conf.RangeChildren(func(path string, element serviceUtils.ConfigurableService) {
//		s.parseRoutes(party.Party(path), element.Router())
//	})
//
//	conf.RangeHandlers(func(method, path string, route interface{}) {
//		if secure, ok := route.(*serviceUtils.SecurePath); ok {
//			if s.rolesMiddleware() == nil {
//				panic("you must provide 'roles' middleware to use this feature")
//			}
//			route = secure.Dest
//			// md := s.rolesMiddleware().SetRole(secure.Role)
//			// party.Party(path).Use(md.MakeIris())
//
//			fmt.Printf(`{ "type": "security", "path": "%s%s", "details": "%s" }`+"\n", party.GetRelPath(), path, secure.Role)
//		}
//
//		s.parseFunc(path, method, party, route)
//	})
//}
//
//func (s *IrisServer) parseFunc(path, method string, party iris.Party, i interface{}) {
//	value := reflect.ValueOf(i)
//	tp := reflect.TypeOf(i)
//
//	if tp.Kind() != reflect.Func {
//		panic("invalid type: " + tp.Kind().String())
//	}
//	fmt.Printf(`{ "type": "route", "path": "%s", "details": "%s" }`+"\n", party.GetRelPath()+path, tp.String())
//
//	if tp.NumIn() == 2 {
//		dest := tp.In(1)
//
//		if method == http.MethodGet {
//			getter := func(bgen.Context) string {
//				return ""
//			}
//
//			name := dest.Name()
//
//			if strings.HasPrefix(name, "path") {
//				getter = utils.GetFromPath(strings.ToLower(strings.TrimPrefix(name, "path")))
//			} else if strings.HasPrefix(name, "query") {
//				getter = utils.GetFromPath(strings.ToLower(strings.TrimPrefix(name, "query")))
//			} else if strings.HasPrefix(name, "header") {
//				getter = utils.GetFromPath(strings.ToLower(strings.TrimPrefix(name, "header")))
//			}
//
//			party.Handle(method, path, func(context context.Context) {
//				ctx := bgen.NewContextFromIris(context)
//				results := value.Call([]reflect.Value{
//					reflect.ValueOf(ctx), reflect.ValueOf(getter(ctx)),
//				})
//				if ctx.IsPushed() {
//					return
//				}
//
//				if !results[1].IsNil() {
//					ctx.ServiceError(results[1].Interface().(serviceUtils.Error))
//				} else {
//					ctx.SetStatus(http.StatusOK)
//					ctx.Response(results[0].Interface())
//				}
//
//				ctx.PushResponse()
//			})
//		} else {
//			party.Handle(method, path, func(context context.Context) {
//				ctx := bgen.NewContextFromIris(context)
//				empty := reflect.New(dest).Interface()
//
//				if err := ctx.ParseBody(empty); err != nil {
//					ctx.SetStatus(http.StatusBadRequest)
//					ctx.Response(err)
//				} else {
//					results := value.Call([]reflect.Value{
//						reflect.ValueOf(ctx), reflect.ValueOf(empty).Elem(),
//					})
//					if ctx.IsPushed() {
//						return
//					}
//
//					if !results[1].IsNil() {
//						ctx.ServiceError(results[1].Interface().(serviceUtils.Error))
//					} else {
//						ctx.SetStatus(http.StatusOK)
//						ctx.Response(results[0].Interface())
//					}
//				}
//
//				ctx.PushResponse()
//			})
//		}
//	} else if tp.NumIn() == 1 {
//		party.Handle(method, path, func(context context.Context) {
//			ctx := bgen.NewContextFromIris(context)
//
//			results := value.Call([]reflect.Value{
//				reflect.ValueOf(ctx),
//			})
//			if !results[1].IsNil() {
//				ctx.ServiceError(results[1].Interface().(serviceUtils.Error))
//			} else {
//				ctx.SetStatus(http.StatusOK)
//				ctx.Response(results[0].Interface())
//			}
//
//			ctx.PushResponse()
//		})
//		tp = nil
//	} else {
//		panic("Invalid count of in args: " + tp.String())
//	}
//}
