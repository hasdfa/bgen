package serverUtils

//func (s *StdHttpServer) parseRoutes(sub muxie.SubMux, conf *serviceUtils.ServiceConfiguration) {
//	conf.RangeMiddleware(func(middleware string) {
//		if v, ok := s.DefinedMiddlewares[middleware]; ok {
//			sub.Use(v.MakeMuxie())
//			fmt.Printf(`{ "type": "middleware", "path": "%s/*", "details": "%s" }`+"\n", sub.AbsPath(), middleware)
//		} else {
//			panic("middleware '" + middleware + "' must be provided")
//		}
//	})
//
//	conf.RangeHandlers(func(method, path string, route interface{}) {
//		if secure, ok := route.(*serviceUtils.SecurePath); ok {
//			if s.rolesMiddleware() == nil {
//				panic("you must provide 'roles' middleware to use this feature")
//			}
//			route = secure.Dest
//			md := s.rolesMiddleware().SetRole(secure.Role)
//			sub.Of(path).Use(md.MakeMuxie())
//
//			fmt.Printf(`{ "type": "security", "path": "%s%s", "details": "%s" }`+"\n", sub.AbsPath(), path, secure.Role)
//		}
//
//		s.parseFunc(path, method, sub, route)
//	})
//
//	conf.RangeChildren(func(path string, element serviceUtils.ConfigurableService) {
//		s.parseRoutes(sub.Of(path), element.Router())
//	})
//}

//func (s *StdHttpServer) parseFunc(path, method string, sub muxie.SubMux, i interface{}) {
//	value := reflect.ValueOf(i)
//	tp := reflect.TypeOf(i)
//
//	if tp.Kind() != reflect.Func {
//		panic("invalid type: " + tp.Kind().String())
//	}
//
//	fmt.Printf(`{ "type": "route", "path": "%s", "details": "%s" }`+"\n", sub.AbsPath()+path, tp.String())
//
//	if tp.NumIn() == 2 {
//		dest := tp.In(1)
//
//		sub.HandleFunc(path, func(w http.ResponseWriter, r *http.Request) {
//			context := bgen.NewContext(w, r)
//
//			if r.Method != method {
//				context.SetStatus(http.StatusMethodNotAllowed)
//				context.PushResponse()
//				return
//			}
//
//			defer func() {
//				if err := recover(); err != nil {
//					if !context.IsPushed() {
//						context.SetStatus(http.StatusInternalServerError)
//						context.Response(err)
//						context.PushResponse()
//					}
//				}
//			}()
//
//			empty := reflect.New(dest).Interface()
//			if err := context.ParseBody(empty); err != nil {
//				context.SetStatus(http.StatusBadRequest)
//				context.Response(err)
//			} else {
//				results := value.Call([]reflect.Value{
//					reflect.ValueOf(context), reflect.ValueOf(empty).Elem(),
//				})
//				if context.IsPushed() {
//					return
//				}
//
//				if !results[1].IsNil() {
//					context.ServiceError(results[1].Interface().(serviceUtils.Error))
//				} else {
//					context.SetStatus(http.StatusOK)
//					context.Response(results[0].Interface())
//				}
//			}
//
//			context.PushResponse()
//		})
//	} else if tp.NumIn() == 1 {
//		sub.HandleFunc(path, func(w http.ResponseWriter, r *http.Request) {
//			context := bgen.NewContext(w, r)
//			defer func() {
//				if err := recover(); err != nil {
//					if !context.IsPushed() {
//						context.SetStatus(http.StatusInternalServerError)
//						context.Response(err)
//						context.PushResponse()
//					}
//				}
//			}()
//
//			results := value.Call([]reflect.Value{
//				reflect.ValueOf(context),
//			})
//			if !results[1].IsNil() {
//				context.ServiceError(results[1].Interface().(serviceUtils.Error))
//			} else {
//				context.SetStatus(http.StatusOK)
//				context.Response(results[0].Interface())
//			}
//
//			context.PushResponse()
//		})
//	} else {
//		panic("Invalid count of in args: " + tp.String())
//	}
//}
