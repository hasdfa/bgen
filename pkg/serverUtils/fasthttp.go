package serverUtils

import (
	"bitbucket.org/hasdfa/bgen/pkg/configUtils"
	"errors"
	"fmt"
	"github.com/buaazp/fasthttprouter"
	"github.com/valyala/fasthttp"
	"net/http"
	"sync"
)

type FastHttpServer struct {
	BaseServer
	Initializer func(*fasthttprouter.Router, *BaseServer)

	router  *fasthttprouter.Router
	servers []*fasthttp.Server
	wg      *sync.WaitGroup
	logMask string
}

func NewFastHttpServer() *FastHttpServer {
	return &FastHttpServer{
		wg:      &sync.WaitGroup{},
		logMask: `{ "server": "http:", "path": "%s" }` + "\n",
	}
}

func (s *FastHttpServer) InitRouter() {
	s.router = fasthttprouter.New()

	if s.Initializer == nil {
		panic("Router must be defined")
	}

	s.Initializer(s.router, &s.BaseServer)
}

func (s *FastHttpServer) Start(port *configUtils.ServerPort) error {
	s.wg.Add(1)
	addr := fmt.Sprintf(":%d", port.Value)
	server := &fasthttp.Server{
		Handler: s.router.Handler,
	}

	if port.Tls != nil {
		fmt.Println("Started http server with tls at " + addr)
		return server.ListenAndServeTLS(addr, port.Tls.CertFile, port.Tls.KeyFile)
	}

	fmt.Println("Started http server at " + addr)
	return server.ListenAndServe(addr)
}

func (s *FastHttpServer) Wait() {
	s.wg.Wait()
}

func (s *FastHttpServer) Logger(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf(s.logMask, r.RequestURI)
		next.ServeHTTP(w, r)
	})
}

func (s *FastHttpServer) Stop(addr uint16) error {
	return errors.New("not implemented")
}

func (s *FastHttpServer) StopAll() (err error) {
	return errors.New("not implemented")
}
