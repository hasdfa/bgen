package serverUtils

import (
	"bitbucket.org/hasdfa/bgen/pkg/configUtils"
	"context"
	"errors"
	"fmt"
	"github.com/kataras/iris"
	"sync"
)

type IrisServer struct {
	BaseServer
	app         *iris.Application
	Initializer func(*iris.Application, *BaseServer)

	wg *sync.WaitGroup
}

func NewIrisServer() *IrisServer {
	return &IrisServer{
		wg:  &sync.WaitGroup{},
		app: iris.Default(),
	}
}

func (s *IrisServer) InitRouter() {
	if s.Initializer == nil {
		panic("Router must be defined")
	}

	s.Initializer(s.app, &s.BaseServer)
}

func (s *IrisServer) Start(port *configUtils.ServerPort) error {
	s.wg.Add(1)
	portString := fmt.Sprintf(":%d", port.Value)

	if port.Tls != nil {
		fmt.Println("Started http server with tls at " + portString)
		return s.app.Run(iris.TLS(portString, port.Tls.CertFile, port.Tls.KeyFile))
	}

	fmt.Println("Started http server at " + portString)
	return s.app.Run(iris.Addr(portString))
}

func (s *IrisServer) Wait() {
	s.wg.Wait()
}

func (s *IrisServer) Stop(addr uint16) error {
	return errors.New("not implemented")
}

func (s *IrisServer) StopAll() (err error) {
	fmt.Println("Shutting down server...")
	return s.app.Shutdown(context.Background())
}
