package serverUtils

import (
	"github.com/buaazp/fasthttprouter"
	"github.com/valyala/fasthttp"
	"path"
)

type fasthttprouterGroup struct {
	p          string
	router     *fasthttprouter.Router
	middleware func(handler fasthttp.RequestHandler) fasthttp.RequestHandler
}

func groupOf(router *fasthttprouter.Router) *fasthttprouterGroup {
	return &fasthttprouterGroup{
		p:      "/",
		router: router,
	}
}

func (r *fasthttprouterGroup) Of(p string) *fasthttprouterGroup {
	return &fasthttprouterGroup{
		p:      path.Join(r.p, p),
		router: r.router,
		// middleware: r.middleware,
	}
}

func (r *fasthttprouterGroup) Handle(method, p string, handler fasthttp.RequestHandler) {
	if r.middleware != nil {
		r.router.Handle(method, path.Join(r.p, p), r.middleware(handler))
	} else {
		r.router.Handle(method, path.Join(r.p, p), handler)
	}
}
