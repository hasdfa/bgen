package serverUtils

import (
	"bitbucket.org/hasdfa/bgen/pkg/configUtils"
	"bitbucket.org/hasdfa/bgen/pkg/utils"
	"errors"
	"fmt"
	"github.com/kataras/muxie"
	"net/http"
	"strconv"
	"strings"
	"sync"
	"time"
)

type StdHttpServer struct {
	BaseServer
	Initializer func(*muxie.Mux, *BaseServer)

	mux     *muxie.Mux
	servers []*http.Server
	wg      *sync.WaitGroup
	logMask string
}

func NewStdHttpServer() *StdHttpServer {
	return &StdHttpServer{
		wg:      &sync.WaitGroup{},
		logMask: `{ "server": "http:", "path": "%s", "status": "%d", "duration": "%s" }` + "\n",
	}
}

func (s *StdHttpServer) InitRouter() {
	s.mux = muxie.NewMux()
	s.mux.PathCorrection = true
	s.mux.Use(s.Logger)

	if s.Initializer == nil {
		panic("Router must be defined")
	}

	s.Initializer(s.mux, &s.BaseServer)
}

func (s *StdHttpServer) Start(port *configUtils.ServerPort) error {
	s.wg.Add(1)
	server := &http.Server{
		Addr:    fmt.Sprintf(":%d", port.Value),
		Handler: s.mux,
	}

	if port.Tls != nil {
		fmt.Println("Started http server with tls at " + server.Addr)
		return server.ListenAndServeTLS(port.Tls.CertFile, port.Tls.KeyFile)
	}

	fmt.Println("Started http server at " + server.Addr)
	return server.ListenAndServe()
}

func (s *StdHttpServer) Wait() {
	s.wg.Wait()
}

func (s *StdHttpServer) Logger(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		custom := &utils.CustomResponseWriter{Default: w}
		next.ServeHTTP(custom, r)

		fmt.Printf(s.logMask, r.RequestURI, custom.Status, time.Now().Sub(start).String())
	})
}

func (s *StdHttpServer) Stop(addr uint16) error {
	for _, srv := range s.servers {
		if strings.HasSuffix(srv.Addr, ":"+strconv.Itoa(int(addr))) {
			return srv.Close()
		}
	}
	return errors.New("unknown server port")
}

func (s *StdHttpServer) StopAll() (err error) {
	for _, srv := range s.servers {
		if serr := srv.Close(); serr != nil {
			err = serr
		}
	}
	return err
}
