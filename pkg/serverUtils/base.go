package serverUtils

import (
	"bitbucket.org/hasdfa/bgen"
	"bitbucket.org/hasdfa/bgen/pkg/middlewareUtils"
	"bitbucket.org/hasdfa/bgen/pkg/serviceUtils"
	"github.com/buaazp/fasthttprouter"
	"github.com/kataras/iris"
	"github.com/kataras/muxie"
)

type (
	BaseServer struct {
		Routes             serviceUtils.ConfigurableService
		DefinedMiddlewares middlewareUtils.MiddlewareDictionary
	}
)

var (
	GenInitStdRouter      func(*muxie.Mux, *BaseServer)
	GenInitIrisRouter     func(*iris.Application, *BaseServer)
	GenInitFasthttpRouter func(*fasthttprouter.Router, *BaseServer)
)

func (s *BaseServer) rolesMiddleware() *middlewareUtils.MiddlewareSource {
	return middlewareUtils.RolesFrom(s.DefinedMiddlewares)
}

func NilHandler(_ bgen.Context) (interface{}, serviceUtils.Error) {
	return nil, serviceUtils.ErrorNotImplemented
}
