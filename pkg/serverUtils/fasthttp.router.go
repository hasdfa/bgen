package serverUtils

//func (s *FastHttpServer) parseRoutes(sub *fasthttprouterGroup, conf *serviceUtils.ServiceConfiguration) {
//	if sub.middleware == nil {
//		sub.middleware = func(handler fasthttp.RequestHandler) fasthttp.RequestHandler {
//			return handler
//		}
//	}
//
//	conf.RangeMiddleware(func(middleware string) {
//		if v, ok := s.DefinedMiddlewares[middleware]; ok {
//			sub.middleware = func(handler fasthttp.RequestHandler) fasthttp.RequestHandler {
//				return sub.middleware(v.MakeFasthttp(handler))
//			}
//			fmt.Printf(`{ "type": "middleware", "path": "%s/*", "details": "%s" }`+"\n", sub.p, middleware)
//		} else {
//			panic("middleware '" + middleware + "' must be provided")
//		}
//	})
//
//	conf.RangeHandlers(func(method, path string, route interface{}) {
//		if secure, ok := route.(*serviceUtils.SecurePath); ok {
//			if s.rolesMiddleware() == nil {
//				panic("you must provide 'roles' middleware to use this feature")
//			}
//			route = secure.Dest
//			md := s.rolesMiddleware().SetRole(secure.Role)
//			sub.Of(path).Handle(method, path, md.MakeFasthttp(s.parseFunc(method, path, sub, route)))
//
//			fmt.Printf(`{ "type": "security", "path": "%s%s", "details": "%s" }`+"\n", sub.p, path, secure.Role)
//		} else {
//			sub.Handle(method, path, s.parseFunc(method, path, sub, route))
//		}
//	})
//
//	conf.RangeChildren(func(path string, element serviceUtils.ConfigurableService) {
//		s.parseRoutes(sub.Of(path), element.Router())
//	})
//}
//
//func (s *FastHttpServer) parseFunc(method, path string, sub *fasthttprouterGroup, i interface{}) fasthttp.RequestHandler {
//	value := reflect.ValueOf(i)
//	tp := reflect.TypeOf(i)
//
//	if tp.Kind() != reflect.Func {
//		panic("invalid type: " + tp.Kind().String())
//	}
//
//	fmt.Printf(`{ "type": "route", "path": "%s", "details": "%s" }`+"\n", sub.p+path, tp.String())
//
//	if tp.NumIn() == 2 {
//		dest := tp.In(1)
//
//		return func(ctx *fasthttp.RequestCtx) {
//			context := bgen.NewFasthttpContext(ctx)
//			defer func() {
//				if err := recover(); err != nil {
//					if !context.IsPushed() {
//						context.SetStatus(http.StatusInternalServerError)
//						context.Response(err)
//						context.PushResponse()
//					}
//				}
//			}()
//
//			empty := reflect.New(dest).Interface()
//			if err := context.ParseBody(empty); err != nil {
//				context.SetStatus(http.StatusBadRequest)
//				context.Response(err)
//			} else {
//				results := value.Call([]reflect.Value{
//					reflect.ValueOf(context), reflect.ValueOf(empty).Elem(),
//				})
//				if context.IsPushed() {
//					return
//				}
//
//				if !results[1].IsNil() {
//					context.ServiceError(results[1].Interface().(serviceUtils.Error))
//				} else {
//					context.SetStatus(http.StatusOK)
//					context.Response(results[0].Interface())
//				}
//			}
//
//			context.PushResponse()
//		}
//	} else if tp.NumIn() == 1 {
//		return func(ctx *fasthttp.RequestCtx) {
//			context := bgen.NewFasthttpContext(ctx)
//
//			defer func() {
//				if err := recover(); err != nil {
//					if !context.IsPushed() {
//						context.SetStatus(http.StatusInternalServerError)
//						context.Response(err)
//						context.PushResponse()
//					}
//				}
//			}()
//
//			results := value.Call([]reflect.Value{
//				reflect.ValueOf(context),
//			})
//			if !results[1].IsNil() {
//				context.ServiceError(results[1].Interface().(serviceUtils.Error))
//			} else {
//				context.SetStatus(http.StatusOK)
//				context.Response(results[0].Interface())
//			}
//
//			context.PushResponse()
//		}
//	} else {
//		panic("Invalid count of in args: " + tp.String())
//	}
//}
