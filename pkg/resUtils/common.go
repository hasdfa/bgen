package resUtils

import (
	"io/ioutil"
	"path"
)

type Repository interface {
	Get(key string) ([]byte, error)
	Abs(key string) string

	Child(dir string) Repository
}

type repo struct {
	path string
}

func NewRepository(abs string) Repository {
	return &repo{
		path: abs,
	}
}

func (rep *repo) Abs(key string) string {
	return path.Join(rep.path, key)
}

func (rep *repo) Get(key string) ([]byte, error) {
	return ioutil.ReadFile(rep.Abs(key))
}

func (rep *repo) Child(dir string) Repository {
	return NewRepository(rep.Abs(dir))
}
