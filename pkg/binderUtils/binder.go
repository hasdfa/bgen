package binderUtils

type Marshaller interface {
	Marshall() ([]byte, error)
}

type Unmarshaler interface {
	Unmarshal([]byte) error
}