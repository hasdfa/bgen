package bgen

import (
	"bitbucket.org/hasdfa/bgen/pkg/dbUtils"
	"bitbucket.org/hasdfa/bgen/pkg/httpUtils"
)

var (
	SharedRepositories = dbUtils.EmptyDatabaseRepository()
)

var (
	DefaultResponseContentType = httpUtils.MIMETextPlain
)
